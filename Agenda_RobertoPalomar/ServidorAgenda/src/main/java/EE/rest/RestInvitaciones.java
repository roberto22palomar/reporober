package EE.rest;

import io.vavr.control.Either;
import modelos.Invitacion;
import servicios.ServiciosInvitaciones;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */
@Path("/invitaciones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestInvitaciones {

    private ServiciosInvitaciones si = new ServiciosInvitaciones();

    @POST
    public Invitacion addInvitacion(Invitacion invitacion) {

        AtomicReference<Invitacion> resultado = new AtomicReference<>();

        si.addInvitacion(invitacion).peek(invitacion1 -> {
            resultado.set(invitacion1);
        }).peekLeft(s -> {
            resultado.set(null);
        });
        return resultado.get();
    }

    @GET
    @Path("/getInvitacionesUsuario")
    public List<Invitacion> getInvitacionesUsuario(@QueryParam("idUsuario") int idUsuario) {
        return si.getInvitacionesUsuario(idUsuario).get();
    }

    @PUT
    public Either<String, Invitacion> updateInvitacion(Invitacion invitacion) {
        return si.updateInvitacion(invitacion);
    }


}
