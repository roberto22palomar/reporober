package EE.rest;

import modelos.Cita;
import modelos.CitaCifrada;
import modelos.Invitacion;
import servicios.ServiciosCitas;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * @author : Rober Palomar
 */
@Path("/citas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestCitas {

    @POST
    public CitaCifrada addCita(CitaCifrada cita) {
        ServiciosCitas sc = new ServiciosCitas();
        return sc.addCita(cita).get();

    }

    @GET
    @Path("/getCitasUsuario")
    public List<CitaCifrada> getCitasUsuario(@QueryParam("idUsuario") int idUsuario) {
        ServiciosCitas sc = new ServiciosCitas();
        return sc.getCitasUsuario(idUsuario).get();
    }

    @POST
    @Path("/getCitasInvitado")
    public List<CitaCifrada> getCitasInvitado(List<Invitacion> invitaciones){
        ServiciosCitas sc = new ServiciosCitas();
        return sc.getCitasInvitado(invitaciones).get();
    }


}
