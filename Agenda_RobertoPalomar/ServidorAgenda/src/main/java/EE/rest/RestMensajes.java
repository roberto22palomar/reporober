package EE.rest;

import dao.DaoMensajes;
import modelos.Mensaje;
import servicios.ServiciosMensajes;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */
@Path("/mensajes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestMensajes {

    private ServiciosMensajes serviciosMensajes = new ServiciosMensajes();

    @POST
    public Mensaje addMensaje(Mensaje mensaje) {

        AtomicReference<Mensaje> resultado = new AtomicReference<>();

        serviciosMensajes.addMensaje(mensaje).peek(mensajeAdd -> {
            resultado.set(mensajeAdd);
        }).peekLeft(s -> {
            resultado.set(null);
        });

        return resultado.get();
    }

    @GET
    @Path("/getMensajesDeUnaCita")
    public List<Mensaje> getMensajesDeUnaCita(@QueryParam("idCita") int idCita) {

        AtomicReference<List<Mensaje>> resultado = new AtomicReference<>();

        serviciosMensajes.getMensajesDeUnaCita(idCita).peek(mensajes -> {
            resultado.set(mensajes);
        }).peekLeft(s -> {
            resultado.set(null);
        });

        return resultado.get();
    }


}
