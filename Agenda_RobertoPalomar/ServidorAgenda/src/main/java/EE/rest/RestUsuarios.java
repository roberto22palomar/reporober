package EE.rest;

import modelos.PeticionDeLogin;
import modelos.Usuario;
import modelos.UsuarioRegistro;
import servicios.ServiciosUsuario;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestUsuarios {


    @GET
    @Path("/byNombre")
    public Usuario getUsuario(@QueryParam("user") String user) {
        ServiciosUsuario su = new ServiciosUsuario();

        return su.getUsuario(user).get();

    }

    @GET
    @Path("/all")
    public List<Usuario> getAllUsuarios() {
        ServiciosUsuario su = new ServiciosUsuario();

        return su.getAllUsuarios().get();

    }

    @POST
    @Path("/getPublica")
    public String getPublica(Usuario usuario) {
        ServiciosUsuario su = new ServiciosUsuario();

        return su.mandarPublicDeInvitado(usuario);

    }

    @POST
    @Path("/login")
    public PeticionDeLogin login(PeticionDeLogin peticionDeLogin, @Context HttpServletRequest httpServletRequest) {
        ServiciosUsuario su = new ServiciosUsuario();

        String pathClavesServidor = httpServletRequest.getServletContext().getRealPath("/WEB-INF/");
        return su.login(peticionDeLogin, pathClavesServidor).get();


    }

    @POST
    public UsuarioRegistro registrarUsuario(UsuarioRegistro usuarioRegistro, @Context HttpServletRequest httpServletRequest) {
        ServiciosUsuario su = new ServiciosUsuario();
        AtomicReference<UsuarioRegistro> resultado = new AtomicReference<>();

        String pathClavesServidor = httpServletRequest.getServletContext().getRealPath("/WEB-INF/");

        su.registrarUsuario(usuarioRegistro, pathClavesServidor).peek(usu -> {
            resultado.set(usu);
        }).peekLeft(s -> {
            resultado.set(null);
        });

        return resultado.get();
    }
}


