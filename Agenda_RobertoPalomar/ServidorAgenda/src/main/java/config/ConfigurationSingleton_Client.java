package config;

import lombok.Getter;
import lombok.Setter;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

@Getter
@Setter
public class ConfigurationSingleton_Client {

    private String ruta;
    private String pass;
    private String user;
    private String driver;
    private static ConfigurationSingleton_Client config;


    private ConfigurationSingleton_Client() {

    }

   public static ConfigurationSingleton_Client cargarInstance(InputStream file) {

        if (config == null) {
            try {
                Yaml yaml = new Yaml();
                config = yaml.loadAs(file,
                        ConfigurationSingleton_Client.class);
            } catch (Exception ex) {
                Logger.getLogger(ConfigurationSingleton_Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return config;
    }

    public static ConfigurationSingleton_Client getInstance() {
        return config;
    }
}
