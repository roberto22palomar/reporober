package servicios;

import dao.DaoUsuarios;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.PeticionDeLogin;
import modelos.Usuario;
import modelos.UsuarioRegistro;
import servicios.utils.CertificadoUtils;
import servicios.utils.KeyStoreUtil;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.List;

/**
 * @author : Rober Palomar
 */

@Log4j2
public class ServiciosUsuario {

    DaoUsuarios du = new DaoUsuarios();

    //REGISTRAR NUEVO USUARIO
    public Either<String, UsuarioRegistro> registrarUsuario(UsuarioRegistro usuario, String pathClavesServidor) {

        CertificadoUtils certificadoUtils = new CertificadoUtils();

        try {

            //DECODIFICAR LA PUBLICA DEL USUARIO
            byte[] publicKeyDecoded = Base64.getUrlDecoder().decode(usuario.getPublicKey());

            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicKeyDecoded);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey clavePublica = keyFactory.generatePublic(pubKeySpec);

            //BUSCAR LA PRIVADA DEL SERVIDOR
            KeyStoreUtil keyStoreUtil = new KeyStoreUtil();
            PrivateKey pkServidor = keyStoreUtil.leerKeyStorePrivado(pathClavesServidor + "/Servidor", "servidor").get();

            //GENERAR CERTIFICADO CON LA PUBLICA DEL USUARIO Y LA PRIVADA DEL SERVIDOR
            certificadoUtils.generarCertificado(usuario.getUsuario().getNombre(), "", clavePublica, pkServidor, pathClavesServidor)
                    .peek(rutaUsuario -> {
                        usuario.getUsuario().setRutaPFX(rutaUsuario);
                    });

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        return du.addUsuario(usuario);
    }

    public Either<String, PeticionDeLogin> login(PeticionDeLogin peticionDeLogin, String pathClavesServidor) {
        return du.login(peticionDeLogin, pathClavesServidor);
    }

    public Either<String, Usuario> getUsuario(String user) {
        return du.getUsuario(user);
    }

    public Either<String, List<Usuario>> getAllUsuarios() {
        return du.getAllUsuarios();
    }


    //MANDAR LA CLAVE PUBLICA DEL INVITADO AL CLIENTE
    public String mandarPublicDeInvitado(Usuario usuario) {

        KeyStoreUtil keyStoreUtil = new KeyStoreUtil();

        PublicKey publicKey = keyStoreUtil.leerKeyStorePublicaConRutaCompleta
                (usuario.getRutaPFX()).get();

        return Base64.getUrlEncoder().encodeToString(publicKey.getEncoded());


    }


}
