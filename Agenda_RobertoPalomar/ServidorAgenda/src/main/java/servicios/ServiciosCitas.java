package servicios;

import dao.DaoCitas;
import io.vavr.control.Either;
import modelos.Cita;
import modelos.CitaCifrada;
import modelos.Invitacion;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public class ServiciosCitas {

    private DaoCitas dc = new DaoCitas();

    public Either<String, CitaCifrada> addCita(CitaCifrada cita) {
        return dc.addCita(cita);
    }

    public Either<String, List<CitaCifrada>> getCitasUsuario(int idUsuario) {
        return dc.getCitasUsuario(idUsuario);
    }

    public Either<String, List<CitaCifrada>> getCitasInvitado(List<Invitacion> invitaciones) {
        return dc.getCitasInvitado(invitaciones);
    }


}
