package servicios;

import dao.DaoInvitaciones;
import io.vavr.control.Either;
import modelos.Invitacion;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public class ServiciosInvitaciones {

    private DaoInvitaciones daoInvitaciones = new DaoInvitaciones();

    public Either<String, Invitacion> addInvitacion(Invitacion invitacion) {
        return daoInvitaciones.addInvitacion(invitacion);
    }

    public Either<String, List<Invitacion>> getInvitacionesUsuario(int idUsuario) {
        return daoInvitaciones.getInvitacionesUsuario(idUsuario);
    }

    public Either<String, Invitacion> updateInvitacion(Invitacion invitacion) {
        return daoInvitaciones.updateInvitacion(invitacion);
    }


}
