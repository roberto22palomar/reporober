package servicios;

import dao.DaoMensajes;
import io.vavr.control.Either;
import modelos.Mensaje;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public class ServiciosMensajes {

    public Either<String, List<Mensaje>> getMensajesDeUnaCita(int idCita) {
        DaoMensajes dm = new DaoMensajes();

        return dm.getMensajesDeUnaCita(idCita);

    }

    public Either<String, Mensaje> addMensaje(Mensaje mensaje) {
        DaoMensajes dm = new DaoMensajes();

        return dm.addMensaje(mensaje);

    }


}
