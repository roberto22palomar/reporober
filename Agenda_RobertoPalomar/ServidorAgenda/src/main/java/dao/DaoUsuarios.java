package dao;

import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.PeticionDeLogin;
import modelos.Usuario;
import modelos.UsuarioRegistro;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import servicios.utils.KeyStoreUtil;
import utils.HibernateUtilsSingleton;

import java.security.*;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */

@Log4j2
public class DaoUsuarios {


    //REGISTRAR UN NUEVO USUARIO
    public Either<String, UsuarioRegistro> addUsuario(UsuarioRegistro usuario) {

        Either<String, UsuarioRegistro> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.beginTransaction();
            session.save(usuario.getUsuario());
            session.getTransaction().commit();
            resultado = Either.right(usuario);

        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException) {
                resultado = Either.left("El usuario ya existe en base de datos");
            } else {
                resultado = Either.left(e.getMessage());
            }

            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }

        return resultado;
    }

    // COMPROBAR EL LOGIN
    public Either<String, PeticionDeLogin> login(PeticionDeLogin peticionDeLogin, String pathClavesServidor) {
        AtomicReference<Either<String, PeticionDeLogin>> resultado = new AtomicReference<>();

        KeyStoreUtil keyStoreUtil = new KeyStoreUtil();

        //RECUPERAR LA CLAVE PUBLICA DEL USUARIO
        keyStoreUtil.leerKeyStorePublica(peticionDeLogin.getUsuario().getNombre(), pathClavesServidor).peek(clave -> {

            PublicKey publicKey = clave;

            try {
                Signature sign = Signature.getInstance("SHA256WithRSA");
                sign.initVerify(publicKey); //Clave publica usuario

                //COMPROBAR FIRMA
                if (sign.verify(Base64.getUrlDecoder().decode(peticionDeLogin.getPeticion()))) {

                    //LA FIRMA ES CORRECTA EL CAMPO "ESTADO" SE PONE A TRUE
                    peticionDeLogin.setEstado(true);

                    //YA DE PASO DEVOLVEMOS EL USUARIO ENTERO DE LA BBDD
                    Usuario userBBDD = getUsuario(peticionDeLogin.getUsuario().getNombre()).get();
                    peticionDeLogin.setUsuario(userBBDD);

                    resultado.set(Either.right(peticionDeLogin));

                } else {
                    resultado.set(Either.left("Firma incorrecta"));
                }

            } catch (NoSuchAlgorithmException ex) {
                log.error(ex.getMessage(), ex);
                resultado.set(Either.left(ex.getMessage()));
            } catch (SignatureException ex) {
                log.error(ex.getMessage(), ex);
                resultado.set(Either.left(ex.getMessage()));
            } catch (InvalidKeyException ex) {
                log.error(ex.getMessage(), ex);
                resultado.set(Either.left(ex.getMessage()));
            }


        }).peekLeft(s -> {
            resultado.set(Either.left(s));
        });

        return resultado.get();
    }

    //COGER UN USUARIO POR SU NOMBRE
    public Either<String, Usuario> getUsuario(String user) {

        Either<String, Usuario> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            Usuario a = session.bySimpleNaturalId(Usuario.class).load(user);

            if (a == null) {
                resultado = Either.left("Usuario no encontrado");
            } else {
                resultado = Either.right(a);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }

    //COGER TODOS LOS USUARIOS DE BASE DE DATOS
    public Either<String, List<Usuario>> getAllUsuarios() {

        Either<String, List<Usuario>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            List<Usuario> usuarios = session.createQuery("from Usuario", Usuario.class).getResultList();

            if (usuarios == null) {
                resultado = Either.left("No hay usuario");
            } else {
                resultado = Either.right(usuarios);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }


}
