package dao;

import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Cita;
import modelos.Invitacion;
import modelos.Mensaje;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class DaoMensajes {

    //AÑADIR UN MENSAJE A LA BBDD
    public Either<String, Mensaje> addMensaje(Mensaje mensaje) {

        Either<String, Mensaje> resultado = null;

        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.beginTransaction();
            session.save(mensaje);
            session.getTransaction().commit();
            resultado = Either.right(mensaje);

        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException) {
                resultado = Either.left("El mensaje ya existe en base de datos");
            } else {
                resultado = Either.left(e.getMessage());
            }

            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }

        return resultado;
    }


    public Either<String, List<Mensaje>> getMensajesDeUnaCita(int idCita) {

        Either<String, List<Mensaje>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            List<Mensaje> mensajes = session.createQuery("from Mensaje where idCita.id=:idCita ", Mensaje.class)
                    .setParameter("idCita", idCita).getResultList();

            if (mensajes.isEmpty()) {
                resultado = Either.left("No hay citas de este usuario");
            } else {
                resultado = Either.right(mensajes);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }


}
