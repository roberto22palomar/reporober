package dao;

import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Cita;
import modelos.Invitacion;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class DaoInvitaciones {

    //AÑADIR UNA NUEVA INVITACION DE UN USUARIO A UNA CITA
    public Either<String, Invitacion> addInvitacion(Invitacion invitacion) {

        Either<String, Invitacion> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.beginTransaction();
            session.save(invitacion);
            session.getTransaction().commit();
            resultado = Either.right(invitacion);

        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException) {
                resultado = Either.left("La invitacion ya existe en base de datos");
            } else {
                resultado = Either.left(e.getMessage());
            }

            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }

        return resultado;
    }

    // COGER TODAS LAS INVITACIONES DE UN USUARIO ESPECIFICO
    public Either<String, List<Invitacion>> getInvitacionesUsuario(int idUsuario){
        Either<String, List<Invitacion>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            List<Invitacion> invitaciones = session.createQuery("from Invitacion where idInvitado.id=:idUsuario ", Invitacion.class)
                    .setParameter("idUsuario", idUsuario).getResultList();

            if (invitaciones.isEmpty()) {
                resultado = Either.left("No hay invitaciones de este usuario");
            } else {
                resultado = Either.right(invitaciones);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    // ACTUALIZAR UNA INVITACION, EL ESTADO DE SI ESTÁ ACEPTADA O RECHAZADA
    public Either<String,  Invitacion> updateInvitacion(Invitacion invitacion) {
        Either<String,  Invitacion> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            session.beginTransaction();
            session.update(invitacion);
            session.getTransaction().commit();
            resultado = Either.right(invitacion);

        }catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException)
                resultado = Either.left("Invitacion con datos relacionados");
            else
                resultado = Either.left(e.getMessage());
            log.error(e.getMessage(), e);
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }
}
