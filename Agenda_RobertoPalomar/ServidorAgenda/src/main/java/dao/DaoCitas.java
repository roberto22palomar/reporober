package dao;

import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Cita;
import modelos.CitaCifrada;
import modelos.Invitacion;
import modelos.Usuario;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class DaoCitas {

    //AÑADIR UNA NUEVA CITA
    public Either<String, CitaCifrada> addCita(CitaCifrada cita) {

        Either<String, CitaCifrada> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {
            session.beginTransaction();
            session.save(cita);
            session.getTransaction().commit();
            resultado = Either.right(cita);

        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException) {
                resultado = Either.left("La cita ya existe en base de datos");
            } else {
                resultado = Either.left(e.getMessage());
            }

            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }

        return resultado;
    }

    public Either<String, List<CitaCifrada>> getCitasUsuario(int idUsuario) {

        Either<String, List<CitaCifrada>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            List<CitaCifrada> citas = session.createQuery("from CitaCifrada where idAnfitrion.id=:idUsuario ", CitaCifrada.class)
                    .setParameter("idUsuario", idUsuario).getResultList();

            if (citas.isEmpty()) {
                resultado = Either.left("No hay citas de este usuario");
            } else {
                resultado = Either.right(citas);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }

    //COGER LAS CITAS A LAS QUE ESTA INVITADO UN USUARIO
    public Either<String, List<CitaCifrada>> getCitasInvitado(List<Invitacion> invitaciones) {

        Either<String, List<CitaCifrada>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            List<CitaCifrada> citas = new ArrayList<>();

            for (int i = 0 ; i<invitaciones.size(); i++){

                Invitacion invitacion = invitaciones.get(i);

                citas = session.createQuery("from CitaCifrada where id=:idCita", CitaCifrada.class)
                        .setParameter("idCita", invitacion.getIdCita().getId()).getResultList();


            }
            if (citas.isEmpty()) {
                resultado = Either.left("No hay citas de este usuario");
            } else {
                resultado = Either.right(citas);
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }


}
