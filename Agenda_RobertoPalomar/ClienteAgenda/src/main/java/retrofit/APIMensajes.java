package retrofit;

import modelos.Mensaje;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public interface APIMensajes {

    @POST("api/mensajes/")
    Call<Mensaje> addMensaje(@Body Mensaje mensaje);

    @GET("api/mensajes/getMensajesDeUnaCita/")
    Call<List<Mensaje>> getMensajesDeUnaCita(@Query("idCita") int idCita);


}
