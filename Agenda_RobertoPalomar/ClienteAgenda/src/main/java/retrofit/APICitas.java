package retrofit;

import modelos.Cita;
import modelos.CitaCifrada;
import modelos.Invitacion;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public interface APICitas {

    @POST("api/citas/")
    Call<CitaCifrada> addCita(@Body CitaCifrada cita);

    @POST("api/citas/getCitasInvitado")
    Call<List<CitaCifrada>> getCitasInvitado(@Body List<Invitacion> invitaciones);

    @GET("api/citas/getCitasUsuario")
    Call<List<CitaCifrada>> getCitasUsuario(@Query("idUsuario") int idUsuario);


}
