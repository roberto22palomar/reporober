package retrofit;

import modelos.Cita;
import modelos.Invitacion;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public interface APIInvitaciones {


    @POST("api/invitaciones/")
    Call<Invitacion> addInvitacion(@Body Invitacion invitacion);

    @GET("api/invitaciones/getInvitacionesUsuario")
    Call<List<Invitacion>> getInvitacionesUsuario(@Query("idUsuario") int idUsuario);

    @PUT("api/invitaciones/")
    Call<Invitacion> updateInvitacion(@Body Invitacion invitacion);

}
