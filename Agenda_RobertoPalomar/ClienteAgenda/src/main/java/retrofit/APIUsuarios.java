package retrofit;


import modelos.PeticionDeLogin;
import modelos.Usuario;
import modelos.UsuarioRegistro;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public interface APIUsuarios {


    @GET("api/users/byNombre")
    Call<Usuario> getUsuarioByNombre(@Query("user") String user);

    @GET("api/users/all")
    Call<List<Usuario>> getAllUsuarios();

    @POST("api/users/")
    Call<UsuarioRegistro> registrarUsuario(@Body UsuarioRegistro usuario);

    @POST("api/users/login")
    Call<PeticionDeLogin> login(@Body PeticionDeLogin peticionDeLogin);

    @POST("api/users/getPublica")
    Call<String> getPublica(@Body Usuario usuario);

}
