package gui.controllers;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import lombok.extern.log4j.Log4j2;
import modelos.PeticionDeLogin;
import modelos.Usuario;
import servicios.ServiciosUsuarios;

import java.net.URL;
import java.util.ResourceBundle;

@Log4j2
public class Login implements Initializable {


    Usuario usuario = null;
    @FXML
    private TextField textFieldUser;
    @FXML
    private Alert info;
    @FXML
    private Alert error;
    @FXML
    private CheckBox checkboxRegister;
    @FXML
    private TextField textfieldUserR;
    @FXML
    private TextField textfieldMailR;
    @FXML
    private Label labelUser;
    @FXML
    private Label labelMail;
    @FXML
    private Button botonRegister;
    private PantallaPrincipal principalController;

    ServiciosUsuarios su = new ServiciosUsuarios();

    public void login() {

        if (!textFieldUser.getText().isEmpty()) {

            Usuario u = Usuario.builder()
                    .nombre(textFieldUser.getText())
                    .mail("")
                    .id(0)
                    .build();

            PeticionDeLogin peticionDeLogin = PeticionDeLogin.builder()
                    .usuario(u)
                    .peticion("peticionDeLoginHola")
                    .estado(false)
                    .build();

            su.login(peticionDeLogin).peek(peticion -> {

                if (peticion.isEstado()) {
                    principalController.setUserLogeado(peticion.getUsuario());
                    principalController.preCargaCitas();
                } else {
                    alertError("No se ha podido loguear");
                }

            }).peekLeft(s -> {
                alertError(s);
            });

        } else {
            alertError("Debes rellenar todos los campos");
        }
    }

    public void registrar() {
        if (!textfieldUserR.getText().isEmpty()
                && !textfieldMailR.getText().isEmpty()) {

            Usuario usuario = Usuario.builder()
                    .id(0)
                    .nombre(textfieldUserR.getText())
                    .mail(textfieldMailR.getText())
                    .build();

            su.registrarUsuario(usuario).peek(u -> {
                alertInfo("Se ha añadido correctamente el usuario");
            }).peekLeft(s -> {
                alertError(s);
            });

        } else {
            alertError("Ningun campo puede estar vacio");
        }
    }

    public void checkboxRegister() {
        if (checkboxRegister.isSelected()) {
            textfieldUserR.setVisible(true);
            textfieldMailR.setVisible(true);
            botonRegister.setVisible(true);
            labelUser.setVisible(true);
            labelMail.setVisible(true);

        } else {
            textfieldUserR.setVisible(false);
            textfieldMailR.setVisible(false);
            botonRegister.setVisible(false);
            labelUser.setVisible(false);
            labelMail.setVisible(false);
        }
    }


    private void alertInfo(String alertText) {
        info.setHeaderText("Login");
        info.setContentText(alertText);
        info.showAndWait();
    }

    private void alertError(String alertText) {
        error.setHeaderText("Login");
        error.setContentText(alertText);
        error.showAndWait();
    }

    public void cargarPrincipalController(PantallaPrincipal principal) {
        principalController = principal;
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        info = new Alert(Alert.AlertType.INFORMATION);
        error = new Alert(Alert.AlertType.ERROR);

    }


}
