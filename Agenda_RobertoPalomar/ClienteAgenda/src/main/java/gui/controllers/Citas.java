package gui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import modelos.*;
import servicios.ServiciosCita;
import servicios.ServiciosInvitaciones;
import servicios.ServiciosMensajes;
import servicios.ServiciosUsuarios;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @author : Rober Palomar
 */
public class Citas implements Initializable {
    @FXML
    private ListView<Cita> listviewCitas;
    @FXML
    private TextField textAsunto;
    @FXML
    private TextField textLugar;
    @FXML
    private DatePicker datePicker;

    @FXML
    private ListView<Usuario> listviewUsuarios;

    @FXML
    private ListView<Invitacion> listviewInvitaciones;

    private PantallaPrincipal principalController;

    private Alert error;
    private Alert info;

    @FXML
    private TextField textfieldMensaje;

    @FXML
    private ListView<MensajeDescrifrado> listviewMensajes;
    @FXML
    private ListView<Cita> listviewCitasMensajes;


    public void enviarMensaje() {

        ServiciosMensajes sm = new ServiciosMensajes();

        CitaCifrada cc = CitaCifrada.builder()
                .id(listviewCitasMensajes.getSelectionModel().getSelectedItem().getId())
                .build();

        Mensaje mensaje = Mensaje.builder()
                .idEmisor(principalController.getUserLogeado())
                .idCita(cc)
                .mensaje(textfieldMensaje.getText())
                .build();


        sm.addMensaje(mensaje).peek(mens -> {
            alertInfo("Se ha añadido el mensaje");
        }).peekLeft(s -> {
            alertError("No se ha añadido");
        });


    }


    public void actualizarMensajes() {

        ServiciosMensajes sm = new ServiciosMensajes();

        int idCita = listviewCitasMensajes.getSelectionModel().getSelectedItem().getId();

        List<MensajeDescrifrado> mensajes = sm.getMensajesDeUnaCita(idCita);

        listviewMensajes.getItems().addAll(mensajes);


    }


    public void addCita() {

        ServiciosCita sc = new ServiciosCita();

        Cita cita = Cita.builder()
                .asunto(textAsunto.getText())
                .lugar(textLugar.getText())
                .fechaHora(datePicker.getValue())
                .idAnfitrion(principalController.getUserLogeado())
                .build();

        sc.addCita(cita).peek(c -> {
            alertInfo("Se ha añadido la cita correctamente");
        }).peekLeft(s -> {
            alertError("No se ha podido añadir la cita");
        });


    }

    public void invitar() {

        List<Usuario> usuariosInvitados = new ArrayList<>();

        CitaCifrada cc = CitaCifrada.builder()
                .id(listviewCitas.getSelectionModel().getSelectedItem().getId())
                .build();

        usuariosInvitados.addAll(listviewUsuarios.getSelectionModel().getSelectedItems());

        ServiciosInvitaciones si = new ServiciosInvitaciones();

        for (int i = 0; i < usuariosInvitados.size(); i++) {

            Usuario usuario = usuariosInvitados.get(i);

            Invitacion invitacion = Invitacion.builder()
                    .idAnfitrion(principalController.getUserLogeado())
                    .idInvitado(usuario)
                    .claveSimetrica("")
                    .idCita(cc)
                    .build();

            si.addInvitacion(invitacion, listviewUsuarios.getSelectionModel().getSelectedItem(), listviewCitas.getSelectionModel().getSelectedItem())
                    .peek(invitado->{
                alertInfo("Se ha invitado al usuario: "+usuario.getNombre()+" correctamente a la cita: "+ listviewCitas.getSelectionModel().getSelectedItem().getAsunto());
            }).peekLeft(s->{
                alertError(s);
            });

        }

    }


    public void aceptarInv() {
        ServiciosInvitaciones si = new ServiciosInvitaciones();

        Invitacion invitacionAceptada = listviewInvitaciones.getSelectionModel().getSelectedItem();


        if (invitacionAceptada != null) {
            invitacionAceptada.setEstado(true);

            si.updateInvitacion(invitacionAceptada).peek(invitacionAceptado -> {
                alertInfo("Se ha aceptado la invitacion");
            }).peekLeft(s -> {
                alertError(s);
            });
        } else {
            alertError("Debes seleccionar una invitacion");
        }


    }

    public void rechazarInv() {

        ServiciosInvitaciones si = new ServiciosInvitaciones();

        Invitacion invitacionAceptada = listviewInvitaciones.getSelectionModel().getSelectedItem();


        if (invitacionAceptada != null) {
            invitacionAceptada.setEstado(false);

            si.updateInvitacion(invitacionAceptada).peek(invitacionAceptado -> {
                alertInfo("Se ha denegado la invitacion");
            }).peekLeft(s -> {
                alertError(s);
            });
        } else {
            alertError("Debes seleccionar una invitacion");
        }


    }


    public void cargarCitasUsuario() {

        ServiciosCita sc = new ServiciosCita();

        sc.getCitasUsuario(principalController.getUserLogeado().getId()).peek(citas -> {
            if (!citas.isEmpty()) {
                listviewCitas.getItems().addAll(citas);
            } else {
                alertError("No hay citas del usuario");
            }
        }).peekLeft(s -> {
            alertError(s);
        });


    }


    public void cargarUsuarios() {

        ServiciosUsuarios su = new ServiciosUsuarios();

        listviewUsuarios.getItems().addAll(su.getAllUsuario().get());

    }

    public void cargarInvitaciones() {

        ServiciosInvitaciones si = new ServiciosInvitaciones();

        si.getInvitacionesUsuario(principalController.getUserLogeado()).peek(invitaciones -> {
            listviewInvitaciones.getItems().addAll(invitaciones);
        }).peekLeft(s -> {
            alertError(s);
        });


    }

    public void cargarCitasDelInvitado() {
        ServiciosCita sc = new ServiciosCita();
        ServiciosInvitaciones si = new ServiciosInvitaciones();


        List<Invitacion> invitaciones = si.getInvitacionesUsuario(principalController.getUserLogeado()).get();

        List<Cita> citasDelInvitado = sc.getCitasInvitado(invitaciones).get();

        listviewCitasMensajes.getItems().addAll(citasDelInvitado);


    }

    public void cargarMensajesDeUnaCita() {
        ServiciosMensajes sm = new ServiciosMensajes();

        sm.getMensajesDeUnaCita(listviewCitasMensajes.getSelectionModel().getSelectedItem().getId());


    }


    public void cargarPrincipalController(PantallaPrincipal principal) {
        principalController = principal;
    }

    private void alertInfo(String alertText) {
        info.setHeaderText("Login");
        info.setContentText(alertText);
        info.showAndWait();
    }

    private void alertError(String alertText) {
        error.setHeaderText("Login");
        error.setContentText(alertText);
        error.showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        info = new Alert(Alert.AlertType.INFORMATION);
        error = new Alert(Alert.AlertType.ERROR);

    }
}
