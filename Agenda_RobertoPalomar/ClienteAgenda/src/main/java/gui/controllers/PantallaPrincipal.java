package gui.controllers;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import modelos.Usuario;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PantallaPrincipal implements Initializable {


    public AnchorPane paneLogin;

    public AnchorPane citas;

    @FXML
    public BorderPane root;

    Usuario userLogeado;


    public Usuario getUserLogeado() {
        return userLogeado;
    }

    public void setUserLogeado(Usuario userLogeado) {
        this.userLogeado = userLogeado;
    }

    public void preCargaLogin() {
        if (paneLogin == null) {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource("/fxml/login.fxml"));
            try {
                paneLogin = loaderMenu.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Login loginController = loaderMenu.getController();
            loginController.cargarPrincipalController(this);

        }
        root.setCenter(paneLogin);
    }

    public void preCargaCitas() {
        if (citas == null) {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource("/fxml/citas.fxml"));
            try {
                citas = loaderMenu.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Citas citasController = loaderMenu.getController();
            citasController.cargarPrincipalController(this);
            citasController.cargarCitasUsuario();
            citasController.cargarUsuarios();
            citasController.cargarInvitaciones();
            citasController.cargarCitasDelInvitado();

        }

        root.setCenter(citas);

    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        preCargaLogin();
    }

}
