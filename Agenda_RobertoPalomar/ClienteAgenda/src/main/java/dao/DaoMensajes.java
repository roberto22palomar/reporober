package dao;

import configRetrofit.ConfigurationSingleton_Retrofit;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Mensaje;
import retrofit.APIMensajes;
import retrofit2.Call;
import retrofit2.Response;

import java.util.List;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class DaoMensajes {


    public Either<String, Mensaje> addMensaje(Mensaje mensaje) {
        Either<String, Mensaje> resultado = null;

        APIMensajes mensajesAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIMensajes.class);

        Call<Mensaje> call = mensajesAPI.addMensaje(mensaje);
        try {
            Response<Mensaje> response = call.execute();
            if (response.isSuccessful()) {
                Mensaje changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }

    public Either<String, List<Mensaje>> getMensajesDeUnaCita(int idCita) {
        Either<String, List<Mensaje>> resultado = null;

        APIMensajes mensajesAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIMensajes.class);

        Call<List<Mensaje>> call = mensajesAPI.getMensajesDeUnaCita(idCita);
        try {
            Response<List<Mensaje>> response = call.execute();
            if (response.isSuccessful()) {
                List<Mensaje> changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
            return resultado;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());

        }
        return resultado;
    }


}
