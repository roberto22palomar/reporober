package dao;

import configRetrofit.ConfigurationSingleton_Retrofit;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Cita;
import modelos.CitaCifrada;
import modelos.Invitacion;
import retrofit.APICitas;
import retrofit2.Call;
import retrofit2.Response;

import java.util.List;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class DaoCitas {


    public Either<String, CitaCifrada> addCita(CitaCifrada cita) {
        Either<String, CitaCifrada> resultado = null;

        APICitas citasAPI = ConfigurationSingleton_Retrofit.getInstance().create(APICitas.class);

        Call<CitaCifrada> call = citasAPI.addCita(cita);
        try {
            Response<CitaCifrada> response = call.execute();
            if (response.isSuccessful()) {
                CitaCifrada changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }

    public Either<String, List<CitaCifrada>> getCitasUsuario(int idUsuario) {
        Either<String, List<CitaCifrada>> resultado = null;

        APICitas citasAPI = ConfigurationSingleton_Retrofit.getInstance().create(APICitas.class);

        Call<List<CitaCifrada>> call = citasAPI.getCitasUsuario(idUsuario);
        try {
            Response<List<CitaCifrada>> response = call.execute();
            if (response.isSuccessful()) {
                List<CitaCifrada> changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
            return resultado;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());

        }
        return resultado;
    }

    public Either<String, List<CitaCifrada>> getCitasInvitado(List<Invitacion> invitaciones) {
        Either<String, List<CitaCifrada>> resultado = null;

        APICitas citasAPI = ConfigurationSingleton_Retrofit.getInstance().create(APICitas.class);

        Call<List<CitaCifrada>> call = citasAPI.getCitasInvitado(invitaciones);
        try {
            Response<List<CitaCifrada>> response = call.execute();
            if (response.isSuccessful()) {
                List<CitaCifrada> changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
            return resultado;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());

        }
        return resultado;
    }


}
