package dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import config.ConfigurationSingleton_Client;
import configRetrofit.ConfigurationSingleton_Retrofit;
import dao.utilsDao.ConfigurationSingleton_OkHttpClient;
import io.vavr.control.Either;
import modelos.PeticionDeLogin;
import modelos.Usuario;
import modelos.UsuarioRegistro;
import retrofit.APIUsuarios;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public class DaoUsuarios {

    public Either<String, Usuario> getUsuario(String user) {
        Either<String, Usuario> resultado = null;

        APIUsuarios usuariosAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIUsuarios.class);

        Call<Usuario> call = usuariosAPI.getUsuarioByNombre(user);
        try {
            Response<Usuario> response = call.execute();
            if (response.isSuccessful()) {
                Usuario changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }

    public Either<String, List<Usuario>> getAllUsuarios() {
        Either<String, List<Usuario>> resultado = null;


        APIUsuarios usuariosAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIUsuarios.class);

        Call<List<Usuario>> call = usuariosAPI.getAllUsuarios();
        try {
            Response<List<Usuario>> response = call.execute();
            if (response.isSuccessful()) {
                List<Usuario> changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }

    public Either<String, UsuarioRegistro> registrarUsuario(UsuarioRegistro usuario) {
        Either<String, UsuarioRegistro> resultado = null;

        APIUsuarios usuariosAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIUsuarios.class);

        Call<UsuarioRegistro> call = usuariosAPI.registrarUsuario(usuario);
        try {
            Response<UsuarioRegistro> response = call.execute();
            if (response.isSuccessful()) {
                UsuarioRegistro changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }

    public Either<String, PeticionDeLogin> login(PeticionDeLogin peticionDeLogin) {
        Either<String, PeticionDeLogin> resultado = null;

        APIUsuarios usuariosAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIUsuarios.class);

        Call<PeticionDeLogin> call = usuariosAPI.login(peticionDeLogin);
        try {
            Response<PeticionDeLogin> response = call.execute();
            if (response.isSuccessful()) {
                PeticionDeLogin changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }

    public Either<String, String> getPublica(Usuario usuario) {
        Either<String, String> resultado = null;


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigurationSingleton_Client.getInstance().getPath_base())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(ConfigurationSingleton_OkHttpClient.getInstance())
                .build();

        APIUsuarios usuariosAPI = retrofit.create(APIUsuarios.class);

        Call<String> call = usuariosAPI.getPublica(usuario);
        try {
            Response<String> response = call.execute();
            if (response.isSuccessful()) {
                String changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }


}
