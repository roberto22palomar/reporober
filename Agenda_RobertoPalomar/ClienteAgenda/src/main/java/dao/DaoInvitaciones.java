package dao;

import configRetrofit.ConfigurationSingleton_Retrofit;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Cita;
import modelos.Invitacion;
import modelos.Usuario;
import retrofit.APICitas;
import retrofit.APIInvitaciones;
import retrofit2.Call;
import retrofit2.Response;

import java.util.List;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class DaoInvitaciones {

    public Either<String, Invitacion> addInvitacion(Invitacion invitacion) {
        Either<String, Invitacion> resultado = null;

        APIInvitaciones invitacionesAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIInvitaciones.class);

        Call<Invitacion> call = invitacionesAPI.addInvitacion(invitacion);
        try {
            Response<Invitacion> response = call.execute();
            if (response.isSuccessful()) {
                Invitacion changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }

    public Either<String, List<Invitacion>> getInvitacionesUsuario(Usuario usuario) {
        Either<String, List<Invitacion>> resultado = null;

        APIInvitaciones invitacionesAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIInvitaciones.class);

        Call<List<Invitacion>> call = invitacionesAPI.getInvitacionesUsuario(usuario.getId());
        try {
            Response<List<Invitacion>> response = call.execute();
            if (response.isSuccessful()) {
                List<Invitacion> changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
            return resultado;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());

        }
        return resultado;
    }

    public Either<String, Invitacion> updateInvitacion(Invitacion invitacion) {
        Either<String, Invitacion> resultado = null;

        APIInvitaciones invitacionesAPI = ConfigurationSingleton_Retrofit.getInstance().create(APIInvitaciones.class);

        Call<Invitacion> call = invitacionesAPI.updateInvitacion(invitacion);
        try {
            Response<Invitacion> response = call.execute();
            if (response.isSuccessful()) {
                Invitacion changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }




}
