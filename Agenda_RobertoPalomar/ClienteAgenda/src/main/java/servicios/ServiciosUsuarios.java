package servicios;

import dao.DaoUsuarios;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.DuplaClaves;
import modelos.PeticionDeLogin;
import modelos.Usuario;
import modelos.UsuarioRegistro;
import servicios.utils.CertificadoUtils;
import servicios.utils.CrearParClaves;
import servicios.utils.KeyStoreUtil;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class ServiciosUsuarios {


    public Either<String, Usuario> registrarUsuario(Usuario usuario) {
        AtomicReference<Either<String, Usuario>> resultado = new AtomicReference<>();

        DaoUsuarios du = new DaoUsuarios();
        CrearParClaves utilsClaves = new CrearParClaves();
        CertificadoUtils certificadoUtils = new CertificadoUtils();

        //GENERAR EL PAR DE CLAVES DEL USUARIO
        utilsClaves.generarParClaves().peek(c -> {
            DuplaClaves claves = null;
            claves = c;

            //GENERAR EL CERTIFICADO PARA GUARDARLO EN EL CLIENTE
            certificadoUtils.generarCertificado(
                    usuario.getNombre(),
                    "",
                    claves.getClavePublica(),
                    claves.getClavePrivada());

            //CODIFICAR LA CLAVE PUBLICA EN BASE 64
            String clavePublica = Base64.getUrlEncoder().encodeToString(claves.getClavePublica().getEncoded());

            //CREAR MODELO DE USUARIO REGISTRO CON EL USUARIO Y LA CLAVE PUBLICA
            UsuarioRegistro usuarioRegistro = UsuarioRegistro.builder()
                    .usuario(usuario)
                    .publicKey(clavePublica)
                    .build();

            //MANDARLO AL SERVIDOR
            du.registrarUsuario(usuarioRegistro).peek(usu -> {
                resultado.set(Either.right(usu.getUsuario()));
            }).peekLeft(s -> {
                resultado.set(Either.left(s));

            });

        }).peekLeft(s -> {
            resultado.set(Either.left(s));
        });

        return resultado.get();
    }


    public Either<String, PeticionDeLogin> login(PeticionDeLogin peticionDeLogin) {
        DaoUsuarios du = new DaoUsuarios();
        AtomicReference<Either<String, PeticionDeLogin>> resultado = new AtomicReference<>();
        KeyStoreUtil keyStoreUtil = new KeyStoreUtil();

        //FIRMA
        try {

            //BUSCAR CLAVE PRIVADA DEL USUARIO
            keyStoreUtil.leerKeyStorePrivado(peticionDeLogin.getUsuario()).peek(clave -> {

                PrivateKey privateKey = clave;

                try {

                    //FIRMAR LA PETICION CON LA CLAVE PRIVADA DEL USUARIO
                    Signature sign = Signature.getInstance("SHA256WithRSA");
                    sign.initSign(privateKey);
                    peticionDeLogin.setPeticion(Base64.getUrlEncoder().encodeToString(sign.sign()));

                    du.login(peticionDeLogin).peek(peticion -> {
                        resultado.set(Either.right(peticion));
                    }).peekLeft(s -> {
                        resultado.set(Either.left(s));
                    });

                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }).peekLeft(s -> {
                resultado.set(Either.left("No se ha podido recuperar la clave privada del usuario"));
            });


        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            resultado.set(Either.left(ex.getMessage()));
        }

        return du.login(peticionDeLogin);
    }


    public Either<String, Usuario> getUsuarioByNombre(String user) {
        DaoUsuarios du = new DaoUsuarios();
        return du.getUsuario(user);
    }

    public Either<String, List<Usuario>> getAllUsuario() {
        DaoUsuarios du = new DaoUsuarios();
        return du.getAllUsuarios();
    }

    public Either<String, PublicKey> getPublica(Usuario usuario) {

        AtomicReference<Either<String, PublicKey>> resultado = new AtomicReference<>();
        DaoUsuarios du = new DaoUsuarios();


        du.getPublica(usuario).peek(publica -> {

            //DECODIFICAR LA PUBLICA DEL USUARIO
            byte[] publicKeyDecoded = Base64.getUrlDecoder().decode(publica);

            try {
                X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicKeyDecoded);
                KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                PublicKey clavePublica = keyFactory.generatePublic(pubKeySpec);

                resultado.set(Either.right(clavePublica));

            } catch (NoSuchAlgorithmException e) {
                resultado.set(Either.left(e.getMessage()));
            } catch (InvalidKeySpecException e) {
                resultado.set(Either.left(e.getMessage()));
            }

        }).peekLeft(s -> {
            resultado.set(Either.left(s));
        });

        return resultado.get();
    }


}
