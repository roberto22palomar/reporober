package servicios;

import dao.DaoInvitaciones;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Cita;
import modelos.Invitacion;
import modelos.Usuario;
import servicios.utils.KeyStoreUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class ServiciosInvitaciones {

    private DaoInvitaciones daoInvitaciones = new DaoInvitaciones();

    public Either<String, Invitacion> addInvitacion(Invitacion invitacion, Usuario usuario, Cita cita) {
        AtomicReference<Either<String, Invitacion>> resultado = new AtomicReference<>();

        ServiciosUsuarios su = new ServiciosUsuarios();

        su.getPublica(usuario).peek(publica -> {

            String claveSimetricaCita = cita.getClaveSimetrica();

            try {


                Cipher cifradorRSA = Cipher.getInstance("RSA");

                cifradorRSA.init(Cipher.ENCRYPT_MODE, publica);

                String claveSimetricaCifrada = Base64.getUrlEncoder()
                        .encodeToString(cifradorRSA.doFinal(claveSimetricaCita.getBytes()));

                invitacion.setClaveSimetrica(claveSimetricaCifrada);

                daoInvitaciones.addInvitacion(invitacion).peek(invitacion1 -> {
                    resultado.set(Either.right(invitacion1));
                }).peekLeft(s -> {
                    resultado.set(Either.left(s));
                });

            } catch (Exception ex) {
                log.error(ex.getMessage());
                resultado.set(Either.left(ex.getMessage()));
            }


        }).peekLeft(s -> {
            resultado.set(Either.left(s));
        });

        return resultado.get();
    }

    public Either<String, List<Invitacion>> getInvitacionesUsuario(Usuario usuario) {
        DaoInvitaciones di = new DaoInvitaciones();
        ServiciosCita sc = new ServiciosCita();
        KeyStoreUtil keyStoreUtil = new KeyStoreUtil();

        AtomicReference<Either<String, List<Invitacion>>> resultado = new AtomicReference<>();

        di.getInvitacionesUsuario(usuario).peek(invitaciones -> {

            List<Invitacion> invitacionesUsuario = invitaciones;

            List<Cita> citasDelInvitado = sc.getCitasInvitado(invitacionesUsuario).get();

            PrivateKey clavePrivada = keyStoreUtil.leerKeyStorePrivado(usuario).get();

            for (int i = 0; i < invitaciones.size(); i++) {

                byte[] claveSimetricaCifrada = invitaciones.get(i).getClaveSimetrica().getBytes();

                try {
                    Cipher cifradorRSA = Cipher.getInstance("RSA");

                    cifradorRSA.init(Cipher.DECRYPT_MODE, clavePrivada);

                    String claveSimetricaDescifrada = new String(cifradorRSA.doFinal(
                            Base64.getUrlDecoder().decode(claveSimetricaCifrada)));


                    //TODO SI SON VARIAS CITAS COMO COMPARARLAS LAS MISMAS (?)
                    //AQUI SE COMPARA SI LA CLAVE DE LA CITA , ES IGUAL A LA DE LA INVITACION
                    // (DESCIFRADA CON LA PRIVADA DEL INVITADO)
                    if (claveSimetricaDescifrada.equals(citasDelInvitado.get(0).getClaveSimetrica())) {

                        resultado.set(Either.right(invitacionesUsuario));

                    } else {
                        resultado.set(Either.left("No es el usuario"));
                    }

                } catch (NoSuchAlgorithmException e) {
                    resultado.set(Either.left(e.getMessage()));
                    log.error(e.getMessage(),e);
                } catch (InvalidKeyException e) {
                    resultado.set(Either.left(e.getMessage()));
                    log.error(e.getMessage(),e);
                } catch (NoSuchPaddingException e) {
                    resultado.set(Either.left(e.getMessage()));
                    log.error(e.getMessage(),e);
                } catch (BadPaddingException e) {
                    resultado.set(Either.left(e.getMessage()));
                    log.error(e.getMessage(),e);
                } catch (IllegalBlockSizeException e) {
                    resultado.set(Either.left(e.getMessage()));
                    log.error(e.getMessage(),e);
                }

            }

        });

        return resultado.get();
    }

    //ACTUALIZAR LA INVITACION (ESTADO)
    public Either<String, Invitacion> updateInvitacion(Invitacion invitacion) {
        return daoInvitaciones.updateInvitacion(invitacion);
    }


}
