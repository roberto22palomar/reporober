package servicios.utils;

import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.x509.X509V3CertificateGenerator;

import java.math.BigInteger;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class CertificadoUtils {

    private final KeyStoreUtil keyStoreUtil = new KeyStoreUtil();

    public Either<String, Boolean> generarCertificado(String user, String passwordU, PublicKey
            clavePublica, PrivateKey clavePrivada) {
        AtomicReference<Either<String, Boolean>> resultado = new AtomicReference<>();

        Security.addProvider(new BouncyCastleProvider());
        KeyPairGenerator keyGen = null;

        try {
            keyGen = KeyPairGenerator.getInstance("RSA");
            keyGen.initialize(2048);


            X509V3CertificateGenerator cert1 = new X509V3CertificateGenerator();
            cert1.setSerialNumber(BigInteger.valueOf(1));   //or generate a random number
            cert1.setSubjectDN(new X509Principal("CN=" + user));  //see examples to add O,OU etc
            cert1.setIssuerDN(new X509Principal("CN=" + user)); //same since it is self-signed
            cert1.setPublicKey(clavePublica);
            cert1.setNotBefore(
                    Date.from(LocalDate.now().plus(365, ChronoUnit.DAYS).atStartOfDay().toInstant(ZoneOffset.UTC)));
            cert1.setNotAfter(new Date());
            cert1.setSignatureAlgorithm("SHA1WithRSAEncryption");
            PrivateKey pk = clavePrivada;

            X509Certificate cert = cert1.generateX509Certificate(pk);
            try {

                keyStoreUtil.crearPFX(cert,user,passwordU,pk).peek(ok->{
                    resultado.set(Either.right(ok));
                }).peekLeft(s->{
                    resultado.set(Either.left(s));
                });


            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
                resultado.set(Either.left(ex.getMessage()));
            }

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            resultado.set(Either.left(ex.getMessage()));
        }
        return resultado.get();
    }
}