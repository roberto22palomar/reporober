package servicios.utils;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Usuario;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class KeyStoreUtil {

    public Either<String, Boolean> crearPFX(X509Certificate certificado, String user, String password, PrivateKey clavePrivada) {
        Either<String, Boolean> resultado = null;
        try {

            KeyStore ks = KeyStore.getInstance("PKCS12");
            ks.load(null, null);

            ks.setCertificateEntry("publica", certificado);
            ks.setKeyEntry("privada", clavePrivada, password.toCharArray(), new java.security.cert.Certificate[]{certificado});
            FileOutputStream fos = new FileOutputStream(user + ".pfx");
            ks.store(fos, "".toCharArray());
            fos.close();

            resultado = Either.right(true);

        } catch (IOException e) {
            log.error(e.getMessage(),e);
            resultado = Either.left(e.getMessage());
        } catch (CertificateException e) {
            log.error(e.getMessage(),e);
            resultado = Either.left(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(),e);
            resultado = Either.left(e.getMessage());
        } catch (KeyStoreException e) {
            log.error(e.getMessage(),e);
            resultado = Either.left(e.getMessage());
        }

        return resultado;

    }

    public Either<String, PublicKey> leerKeyStorePublica(String usuario) {
        Either<String, PublicKey> resultado = null;


        try {
            KeyStore ksLoad = KeyStore.getInstance("PKCS12");
            ksLoad.load(new FileInputStream(usuario + ".pfx"), "".toCharArray());

            X509Certificate certLoad = (X509Certificate) ksLoad.getCertificate("publica");

            resultado = Either.right(certLoad.getPublicKey());

        } catch (IOException e) {
            log.error(e.getMessage(),e);
            resultado = Either.left(e.getMessage());
        } catch (CertificateException e) {
            log.error(e.getMessage(),e);
            resultado = Either.left(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(),e);
            resultado = Either.left(e.getMessage());
        } catch (KeyStoreException e) {
            log.error(e.getMessage(),e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }

    public Either<String, PrivateKey> leerKeyStorePrivado(Usuario usuario) {
        Either<String, PrivateKey> resultado = null;

        try {
            KeyStore ksLoad = KeyStore.getInstance("PKCS12");
            ksLoad.load(new FileInputStream(usuario.getNombre() + ".pfx"), "".toCharArray());

            KeyStore.PasswordProtection pt = new KeyStore.PasswordProtection("".toCharArray());
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) ksLoad.getEntry("privada", pt);
            PrivateKey keyLoad = privateKeyEntry.getPrivateKey();

            resultado = Either.right(keyLoad);


        } catch (IOException e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        } catch (CertificateException e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        } catch (UnrecoverableEntryException e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        } catch (KeyStoreException e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;
    }




}
