package servicios.utils;


import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.DuplaClaves;

import java.security.*;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class CrearParClaves {

    public Either<String, DuplaClaves> generarParClaves() {
        Either<String, DuplaClaves> parDeClaves = null;

        KeyPairGenerator generadorRSA = null;
        try {

            generadorRSA = KeyPairGenerator.getInstance("RSA");
            generadorRSA.initialize(2048);
            KeyPair clavesRSA = generadorRSA.generateKeyPair();
            PrivateKey clavePrivada = clavesRSA.getPrivate();
            PublicKey clavePublica = clavesRSA.getPublic();
            DuplaClaves duplaClaves = DuplaClaves.builder()
                    .clavePublica(clavePublica)
                    .clavePrivada(clavePrivada)
                    .build();

            parDeClaves = Either.right(duplaClaves);

        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
            parDeClaves = Either.left("Error al generar Par de Claves");
        }

        return parDeClaves;

    }



}
