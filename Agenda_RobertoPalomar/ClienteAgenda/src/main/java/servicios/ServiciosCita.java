package servicios;

import com.google.gson.Gson;
import dao.DaoCitas;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Cita;
import modelos.CitaCifrada;
import modelos.Invitacion;
import servicios.utils.KeyStoreUtil;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class ServiciosCita {

    private DaoCitas dc = new DaoCitas();

    public Either<String, Cita> addCita(Cita cita) {

        SecureRandom sr = new SecureRandom();
        Gson gson = new Gson();
        CitaCifrada citaCifrada = null;
        AtomicReference<Either<String, Cita>> resultado = new AtomicReference<>();

        byte[] clave = new byte[16];
        byte[] iv = new byte[12];
        byte[] salt = new byte[16];

        sr.nextBytes(clave);
        String claveSimetrica = Base64.getUrlEncoder().encodeToString(clave);

        sr.nextBytes(iv);
        sr.nextBytes(salt);


        String citaJson = gson.toJson(cita);


        try {


            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(claveSimetrica.toCharArray(), salt, 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");


            Cipher cipher = Cipher.getInstance("AES/GCM/noPadding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);

            citaCifrada = CitaCifrada.builder()
                    .id(0)
                    .idAnfitrion(cita.getIdAnfitrion())
                    .iv(Base64.getUrlEncoder().encodeToString(iv))
                    .salt(Base64.getUrlEncoder().encodeToString(salt))
                    .iteraciones(65536)
                    .claveSimetrica(Base64.getUrlEncoder().encodeToString(claveSimetrica.getBytes()))
                    .cita(Base64.getUrlEncoder().encodeToString(
                            cipher.doFinal(citaJson.getBytes())))
                    .build();

            KeyStoreUtil keyStoreUtil = new KeyStoreUtil();


            CitaCifrada finalCitaCifrada = citaCifrada;
            keyStoreUtil.leerKeyStorePrivado(cita.getIdAnfitrion()).peek(clavePrivada -> {


                //FIRMAR
                Signature sign = null;
                try {
                    sign = Signature.getInstance("SHA256WithRSA");
                    sign.initSign(clavePrivada);
                    sign.update(finalCitaCifrada.getCita().getBytes());
                    finalCitaCifrada.setFirma(Base64.getUrlEncoder().encodeToString(sign.sign()));


                    //SE SUBE A BBDD EL MENSAJE YA COMPLETO
                    DaoCitas dc = new DaoCitas();

                    dc.addCita(finalCitaCifrada).peek(citaCifrada1 -> {
                        //SUBIDO A BBDD
                        resultado.set(Either.right(cita));
                    }).peekLeft(s -> {
                        resultado.set(Either.left(s));
                        //NO SUBIDO
                    });

                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (SignatureException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }
            }).peekLeft(s -> {
                resultado.set(Either.left(s));
            });


        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }


        return resultado.get();
    }

    public Either<String, List<Cita>> getCitasUsuario(int idUsuario) {
        AtomicReference<Either<String, List<Cita>>> resultado = new AtomicReference<>();

        dc.getCitasUsuario(idUsuario).peek(citas -> {
            List<CitaCifrada> citasCifradas = citas;

            descrifrarCitas(citasCifradas).peek(descrifradas -> {
                resultado.set(Either.right(descrifradas));
            }).peekLeft(s -> {
                resultado.set(Either.left(s));
            });
        }).peekLeft(s->{
            resultado.set(Either.left(s));
        });




        return resultado.get();
    }

    public Either<String, List<Cita>> getCitasInvitado(List<Invitacion> invitaciones) {
        List<CitaCifrada> citasCifradas = dc.getCitasInvitado(invitaciones).get();
        AtomicReference<Either<String, List<Cita>>> resultado = new AtomicReference<>();

        descrifrarCitas(citasCifradas).peek(descrifradas -> {
            resultado.set(Either.right(descrifradas));
        }).peekLeft(s -> {
            resultado.set(Either.left(s));
        });

        return resultado.get();


    }


    public Either<String, List<Cita>> descrifrarCitas(List<CitaCifrada> citasCifradas) {
        Either<String, List<Cita>> resultado = null;
        List<Cita> citasDescifradas = new ArrayList<>();

        try {

            for (int i = 0; i < citasCifradas.size(); i++) {

                CitaCifrada citaCifrada = citasCifradas.get(i);

                byte[] iv = Base64.getUrlDecoder().decode(citaCifrada.getIv());
                byte[] salt = Base64.getUrlDecoder().decode(citaCifrada.getSalt());

                int numeroIteraciones = citaCifrada.getIteraciones();

                String claveSimetricaDecoded = new String(Base64.getUrlDecoder().decode(citaCifrada.getClaveSimetrica().getBytes()));


                //PARAMETROS PARA LA DESENCRIPTACION
                GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
                SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
                KeySpec spec = new PBEKeySpec(claveSimetricaDecoded.toCharArray(), salt, numeroIteraciones, 256);
                SecretKey tmp = factory.generateSecret(spec);
                SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

                //DESCRIFRADOR
                Cipher cipher = Cipher.getInstance("AES/GCM/noPADDING");
                cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);
                String citaDesc = new String(cipher.doFinal(
                        Base64.getUrlDecoder().decode(citaCifrada.getCita().getBytes())));

                Gson gson = new Gson();

                Cita cita = gson.fromJson(citaDesc, Cita.class);

                cita.setId(citaCifrada.getId());
                cita.setClaveSimetrica(citaCifrada.getClaveSimetrica());

                citasDescifradas.add(cita);

                resultado = Either.right(citasDescifradas);
            }

        } catch (InvalidKeySpecException | IllegalBlockSizeException | BadPaddingException e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }

        return resultado;
    }

}
