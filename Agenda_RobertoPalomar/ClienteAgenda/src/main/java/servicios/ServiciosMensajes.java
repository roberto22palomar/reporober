package servicios;

import dao.DaoMensajes;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import modelos.Mensaje;
import modelos.MensajeDescrifrado;
import modelos.Usuario;
import servicios.utils.KeyStoreUtil;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class ServiciosMensajes {


    public Either<String, Mensaje> addMensaje(Mensaje mensaje) {
        AtomicReference<Either<String, Mensaje>> resultado = new AtomicReference<>();

        SecureRandom sr = new SecureRandom();
        try {
            byte[] clave = new byte[16];
            byte[] iv = new byte[12];
            byte[] salt = new byte[16];

            sr.nextBytes(clave);
            String claveSimetrica = Base64.getUrlEncoder().encodeToString(clave);

            sr.nextBytes(iv);
            sr.nextBytes(salt);

            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(claveSimetrica.toCharArray(), salt, 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");


            Cipher cipher = Cipher.getInstance("AES/GCM/noPadding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);

            Mensaje mensajeEncriptado = Mensaje.builder()
                    .id(0)
                    .idEmisor(mensaje.getIdEmisor())
                    .idCita(mensaje.getIdCita())
                    .iv(Base64.getUrlEncoder().encodeToString(iv))
                    .salt(Base64.getUrlEncoder().encodeToString(salt))
                    .iteraciones(65536)
                    .claveSimetrica(Base64.getUrlEncoder().encodeToString(claveSimetrica.getBytes()))
                    .mensaje(Base64.getUrlEncoder().encodeToString(
                            cipher.doFinal(mensaje.getMensaje().getBytes(StandardCharsets.UTF_8))))
                    .build();

            KeyStoreUtil keyStoreUtil = new KeyStoreUtil();


            keyStoreUtil.leerKeyStorePrivado(mensaje.getIdEmisor()).peek(clavePrivada -> {


                //FIRMAR
                Signature sign = null;
                try {
                    sign = Signature.getInstance("SHA256WithRSA");
                    sign.initSign(clavePrivada);
                    sign.update(mensaje.getMensaje().getBytes());
                    mensajeEncriptado.setFirma(Base64.getUrlEncoder().encodeToString(sign.sign()));

                    //SE SUBE A BBDD EL MENSAJE YA COMPLETO
                    DaoMensajes dm = new DaoMensajes();

                    dm.addMensaje(mensajeEncriptado).peek(mensajeEncriptado1 -> {
                        //SUBIDO A BBDD
                        resultado.set(Either.right(mensaje));
                    }).peekLeft(s -> {
                        //NO SUBIDO
                    });

                } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
                    resultado.set(Either.left(e.getMessage()));
                    log.error(e.getMessage(), e);
                }


            });
        } catch (InvalidKeySpecException | IllegalBlockSizeException | BadPaddingException
                | NoSuchAlgorithmException | NoSuchPaddingException
                | InvalidKeyException | InvalidAlgorithmParameterException e) {
            resultado.set(Either.left(e.getMessage()));
            log.error(e.getMessage(), e);
        }

        return resultado.get();

    }

    public List<MensajeDescrifrado> getMensajesDeUnaCita(int idCita) {

        DaoMensajes daoMensajes = new DaoMensajes();
        List<Mensaje> mensajes = daoMensajes.getMensajesDeUnaCita(idCita).get();
        if (mensajes != null || !mensajes.isEmpty()) {

            return descrifrar(mensajes);
        } else {
            return null;
        }
    }

    public List<MensajeDescrifrado> descrifrar(List<Mensaje> mensajes) {

        //DESCIFRAR MENSAJE
        List<MensajeDescrifrado> mensajesDescrifrados = new ArrayList<>();

        for (int i = 0; i < mensajes.size(); i++) {

            Mensaje mensaje = mensajes.get(i);

            byte[] iv = Base64.getUrlDecoder().decode(mensaje.getIv());
            byte[] salt = Base64.getUrlDecoder().decode(mensaje.getSalt());

            int numeroIteraciones = mensaje.getIteraciones();

            String claveSimetricaDecoded = new String(Base64.getUrlDecoder().decode(mensaje.getClaveSimetrica().getBytes()));

            try {
                //PARAMETROS PARA LA DESENCRIPTACION
                GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
                SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
                KeySpec spec = new PBEKeySpec(claveSimetricaDecoded.toCharArray(), salt, numeroIteraciones, 256);
                SecretKey tmp = factory.generateSecret(spec);
                SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

                //CRIFRADOR
                Cipher cipher = Cipher.getInstance("AES/GCM/noPADDING");
                cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);
                String mensajeDescifrado = new String(cipher.doFinal(
                        Base64.getUrlDecoder().decode(mensaje.getMensaje().getBytes(StandardCharsets.UTF_8))));

                ServiciosUsuarios su = new ServiciosUsuarios();

                Usuario emisor = su.getUsuarioByNombre(mensaje.getIdEmisor().getNombre()).get();

                MensajeDescrifrado mensajeDesc = MensajeDescrifrado.builder()
                        .emisor(emisor.getNombre())
                        .mensaje(mensajeDescifrado)
                        .build();

                mensajesDescrifrados.add(mensajeDesc);


            } catch (InvalidKeySpecException | IllegalBlockSizeException | BadPaddingException e) {
                log.error(e.getMessage(), e);
            } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
                log.error(e.getMessage(), e);
            }

        }

        return mensajesDescrifrados;
    }
}