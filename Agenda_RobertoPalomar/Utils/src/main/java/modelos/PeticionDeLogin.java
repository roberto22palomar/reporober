package modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : Rober Palomar
 */

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class PeticionDeLogin {

    private Usuario usuario;

    private String peticion;

    private boolean estado;

}
