package modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : Rober Palomar
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class MensajeDescrifrado {

    private String mensaje;
    private String emisor;

    @Override
    public String toString() {
        return "Emisor = " + emisor + '\n' + "Mesanje = " + mensaje;
    }
}
