package modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author : Rober Palomar
 */

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "Invitaciones")
public class Invitacion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "idAnfitrion", referencedColumnName = "id")
    private Usuario idAnfitrion;
    @ManyToOne
    @JoinColumn(name = "idInvitado", referencedColumnName = "id")
    private Usuario idInvitado;
    @ManyToOne
    @JoinColumn(name = "idCita", referencedColumnName = "id")
    private CitaCifrada idCita;
    @Basic
    @Column(name = "claveSimetrica")
    private String claveSimetrica;
    @Basic
    @Column(name = "estado")
    private boolean estado;


}
