package modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * @author : Rober Palomar
 */

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Cita {

    private int id;
    private String asunto;
    private String lugar;
    private LocalDate fechaHora;
    private Usuario idAnfitrion;
    private String claveSimetrica;


    @Override
    public String toString() {
        return "ID = " + id +
                ", Asunto = " + asunto + '\'' +
                ", Lugar = " + lugar + '\'' +
                ", Fecha = " + fechaHora +
                ", Creador = " + idAnfitrion.getNombre() +
                '}';
    }
}
