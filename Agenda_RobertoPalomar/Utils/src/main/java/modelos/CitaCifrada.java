package modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * @author : Rober Palomar
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "Citas")
public class CitaCifrada {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "cita")
    private String cita;
    @Basic
    @Column(name = "iv")
    private String iv;
    @Basic
    @Column(name = "salt")
    private String salt;
    @Basic
    @Column(name = "firma")
    private String firma;
    @Basic
    @Column(name = "iteraciones")
    private int iteraciones;
    @Basic
    @Column(name = "claveSimetrica")
    private String claveSimetrica;
    @ManyToOne
    @JoinColumn(name = "idAnfitrion", referencedColumnName = "id")
    private Usuario idAnfitrion;



}
