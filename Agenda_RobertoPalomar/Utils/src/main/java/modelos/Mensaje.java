package modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

/**
 * @author : Rober Palomar
 */

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "Mensajes")
public class Mensaje {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(name = "mensaje")
    private String mensaje;

    @ManyToOne
    @JoinColumn(name = "idEmisor", referencedColumnName = "id")
    private Usuario idEmisor;

    @Basic
    @Column(name = "iv")
    private String iv;

    @Basic
    @Column(name = "salt")
    private String salt;

    @Basic
    @Column(name = "claveSimetrica")
    private String claveSimetrica;

    @Basic
    @Column(name = "firma")
    private String firma;

    @Basic
    @Column(name = "iteraciones")
    private int iteraciones;

    @ManyToOne
    @JoinColumn(name = "idCita", referencedColumnName = "id")
    private CitaCifrada idCita;


}
