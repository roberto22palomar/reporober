package dao.modelos;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author : Rober Palomar
 */
@Data
@Builder
public class Usuario {

    @NotBlank
    @NotEmpty
    @NotNull
    private int id;
    @NotBlank
    @NotEmpty
    @NotNull
    private String user;
    @NotBlank
    @NotEmpty
    @NotNull
    private String password;

    @NotNull
    private String mail;

    private boolean multa;



}
