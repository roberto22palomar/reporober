package dao.modelos;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : Rober Palomar
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Libro {

    @NotBlank
    @NotEmpty
    @NotNull
    private int id;
    @NotBlank
    @NotEmpty
    @NotNull
    private String titulo;
    @NotBlank
    @NotEmpty
    @NotNull
    private String autor;
    @NotBlank
    @NotEmpty
    @NotNull
    private String genero;

    private List<Prestamo> prestamos ;
}
