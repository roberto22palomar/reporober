package dao.modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * @author : Rober Palomar
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Prestamo {
    @NotBlank
    @NotEmpty
    @NotNull
    private int idUsuario;
    @NotBlank
    @NotEmpty
    @NotNull
    private int idLibro;

    private LocalDate fechaPrestamo;

    private LocalDate fechaDevolucion;
}
