package dao;

import com.github.javafaker.Faker;
import dao.modelos.Usuario;
import io.vavr.control.Either;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Rober Palomar
 */

public class DaoUsuarios {

    public static ArrayList<Usuario> usuarios = new ArrayList<>();


    public void generarUsuarios() {

        Faker faker = new Faker();

        for (int i = 0; i < 10; i++) {
            Usuario u = Usuario.builder()
                    .id(usuarios.size())
                    .user(faker.artist().name())
                    .password(faker.number().randomDigit() + "")
                    .mail(faker.internet().emailAddress())
                    .multa(false)
                    .build();
            usuarios.add(u);
        }


    }

    public Either<String, Usuario> getUsuarioPorID(int id) {
        Either<String, Usuario> resultado = null;
        Usuario u = usuarios.stream().filter(usuario -> usuario.getId() == id).findFirst().orElse(null);

        if (u != null) {
            resultado = Either.right(u);
        } else {
            resultado = Either.left("No se ha encontrado ningun usuario con ese ID");
        }

        return resultado;
    }

    public Either<String, List<Usuario>> getAllUsuarios() {
        Either<String, List<Usuario>> resultado = null;

        if (usuarios.size() > 1) {
            resultado = Either.right(usuarios);
        } else {
            resultado = Either.left("No hay usuarios");
        }

        return resultado;

    }

    public Either<String, Usuario> addUsuario(Usuario usuario) {
        Either<String, Usuario> resultado = null;

        int size1 = usuarios.size();

        usuarios.add(usuario);

        if (usuarios.size() > size1) {
            resultado = Either.right(usuario);
        } else {
            resultado = Either.left("No se ha añadido al usuario correctamente");
        }
        return resultado;
    }

    public Usuario multar(int idUsuario) {
        usuarios.get(idUsuario).setMulta(true);
        return usuarios.get(idUsuario);
    }

}
