package dao;

import com.github.javafaker.Faker;
import dao.modelos.Libro;
import dao.modelos.Prestamo;
import io.vavr.control.Either;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : Rober Palomar
 */
public class DaoLibros {

    public static ArrayList<Libro> libros = new ArrayList<>();

    public void generarLibros() {

        Faker faker = new Faker();

        for (int i = 0; i < 10; i++) {
            Libro l = Libro.builder()
                    .id(libros.size())
                    .titulo(faker.book().title())
                    .autor(faker.book().author())
                    .genero(faker.book().genre())
                    .prestamos(new ArrayList<>())
                    .build();
            libros.add(l);
        }


    }

    public Either<String, Libro> getLibroPorID(int id) {
        Either<String, Libro> resultado = null;
        Libro l = libros.stream().filter(libro -> libro.getId() == id).findFirst().orElse(null);

        if (l != null) {
            resultado = Either.right(l);
        } else {
            resultado = Either.left("No se ha encontrado ningun libro con ese ID");
        }

        return resultado;
    }

    public Either<String, List<Libro>> getAllLibros() {
        Either<String, List<Libro>> resultado = null;

        if (libros.size() > 1) {
            resultado = Either.right(libros);
        } else {
            resultado = Either.left("No hay libros");
        }

        return resultado;

    }

    public Either<String, Libro> addPrestamo(Prestamo prestamo) {
        Either<String, Libro> resultado = null;

        List<Prestamo> prestamos = new ArrayList<>();
        prestamos.add(prestamo);

        libros.get(prestamo.getIdLibro()).setPrestamos(prestamos);
        if (libros.get(prestamo.getIdLibro()).getPrestamos().size() > 0) {
            resultado = Either.right(libros.get(prestamo.getIdLibro()));
        } else {
            resultado = Either.left("No se ha añadido el prestamo");
        }

        return resultado;
    }

    public List<Prestamo> getPrestamosUsuarios(int idUsuario) {

        List<Prestamo> listaLibrosUsuario = new ArrayList<>();

        libros.stream().forEach(libro -> libro.getPrestamos().forEach(prestamo -> {
            if (prestamo.getIdUsuario() == idUsuario) {
                listaLibrosUsuario.add(prestamo);
            }
        }));

        return listaLibrosUsuario;

    }

    public boolean deletePrestamo(int idUsuario, int idLibro) {

        int size = libros.get(idLibro).getPrestamos().size();

        libros.get(idLibro).getPrestamos().clear();

        if(libros.get(idLibro).getPrestamos().size()<size){
            return true;
        }else{
            return false;
        }

    }


}
