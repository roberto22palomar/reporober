package servicios;

import dao.DaoLibros;
import dao.modelos.Libro;
import dao.modelos.Prestamo;
import io.vavr.control.Either;

import javax.inject.Inject;
import java.util.List;

/**
 * @author : Rober Palomar
 */
public class ServiciosLibros {

    @Inject
    private DaoLibros dl;

    public void generarLibros() {
        dl.generarLibros();
    }

    public Either<String, Libro> getLibroPorID(int id) {
        return dl.getLibroPorID(id);
    }

    public Either<String, List<Libro>> getAllLibros() {
        return dl.getAllLibros();

    }

    public Either<String, Libro> addPrestamo(Prestamo prestamo) {
        return dl.addPrestamo(prestamo);
    }

    public List<Prestamo> getPrestamosUsuarios(int idUsuario) {
        return dl.getPrestamosUsuarios(idUsuario);
    }

    public boolean deletePrestamo(int idUsuario, int idLibro) {
        return dl.deletePrestamo(idUsuario,idLibro);
    }


}
