package servicios;

import dao.DaoUsuarios;
import dao.modelos.Usuario;
import io.vavr.control.Either;

import javax.inject.Inject;
import java.util.List;

/**
 * @author : Rober Palomar
 */
public class ServiciosUsuarios {

    @Inject
    private DaoUsuarios du;

    public void generarUsuarios() {
        du.generarUsuarios();
    }

    public Either<String, Usuario> getUsuarioPorID(int id) {
        return du.getUsuarioPorID(id);
    }

    public Either<String, List<Usuario>> getAllUsuarios() {
        return du.getAllUsuarios();
    }

    public Either<String, Usuario> addUsuario(Usuario usuario) {
        return du.addUsuario(usuario);
    }

    public Usuario multar(int idUsuario) {
        return du.multar(idUsuario);
    }


}
