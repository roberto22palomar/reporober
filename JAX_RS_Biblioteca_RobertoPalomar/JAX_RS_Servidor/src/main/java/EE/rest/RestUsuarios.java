package EE.rest;

import dao.modelos.Usuario;
import org.modelmapper.ModelMapper;
import servicios.ServiciosUsuarios;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestUsuarios {

    @Inject
    private ServiciosUsuarios su;
    @Inject
    private ModelMapper mapper;

    @GET
    @Path("/uno")
    public Response getUsuario(@QueryParam("id") int id,
                               @Context HttpServletRequest request) {

        AtomicReference<Response> r = new AtomicReference();

        su.getUsuarioPorID(id).peek(usuario -> {
            r.set(Response.ok(usuario).build());
        }).peekLeft(apiError -> r.set(Response.status(Response.Status.NOT_FOUND)
                .entity(apiError)
                .build()));

        return r.get();

    }

    @GET
    @Path("/all")
    public List<Usuario> getAllUsuario() {
        su.generarUsuarios();
        return su.getAllUsuarios().get();
    }

    @POST
    @Path("/add")
    public Usuario addUsuario(Usuario user) {
        return su.addUsuario(user).get();
    }


    @PUT
    @Path("/multar")
    public Usuario multar(int idUsuario){
        return su.multar(idUsuario);
    }
}


