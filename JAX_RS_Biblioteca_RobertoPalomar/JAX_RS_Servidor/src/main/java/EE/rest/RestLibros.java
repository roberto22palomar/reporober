package EE.rest;

import dao.modelos.Libro;
import dao.modelos.Prestamo;
import servicios.ServiciosLibros;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */

@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RestLibros {

    @Inject
    private ServiciosLibros sl;

    @GET
    @Path("/uno")
    public Response getUsuario(@QueryParam("id") int id,
                               @Context HttpServletRequest request) {

        AtomicReference<Response> r = new AtomicReference();

        sl.getLibroPorID(id).peek(libro -> {
            r.set(Response.ok(libro).build());
        }).peekLeft(apiError -> r.set(Response.status(Response.Status.NOT_FOUND)
                .entity(apiError)
                .build()));

        return r.get();

    }

    @GET
    @Path("/all")
    public List<Libro> getAllUsuario() {
        sl.generarLibros();
        return sl.getAllLibros().get();
    }

    @POST
    public Libro addPrestamo(Prestamo prestamo) {

        return sl.addPrestamo(prestamo).get();

    }

    @DELETE
    @Path("/delete")
    public boolean deletePrestamo(@QueryParam("idUsuario") int idUsuario, @QueryParam("idLibro") int idLibro, @Context HttpServletRequest request) {
        return sl.deletePrestamo(idLibro,idUsuario);
    }

    @GET
    @Path("/prestamosUsuario")
    public List<Prestamo> getPrestamosUsuario(@QueryParam("idUsuario") int idUsuario) {
        return sl.getPrestamosUsuarios(idUsuario);
    }

}
