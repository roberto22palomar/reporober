package retrofit;

import dao.modelos.Libro;
import dao.modelos.Prestamo;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public interface APILibros {


    @GET("api/books/uno")
    Call<Libro> getLibrosByID(@Query("id") int id);

    @GET("api/books/all")
    Call<List<Libro>> getAllLibros();

    @POST("api/books")
    Call<Libro> addPrestamo(@Body Prestamo prestamo);

    @GET("api/books/prestamosUsuario")
    Call<List<Prestamo>> getPrestamosUsuario(@Query("idUsuario") int idUsuario);

    @DELETE("api/books/delete")
    Call<Boolean> deletePrestamo(@Query("idUsuario") int idUsuario,@Query("idLibro") int idLibro);



}
