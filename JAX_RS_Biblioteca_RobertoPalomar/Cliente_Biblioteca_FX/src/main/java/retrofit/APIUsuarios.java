package retrofit;


import dao.modelos.Usuario;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public interface APIUsuarios {


    @GET("api/users/uno")
    Call<Usuario> getUsuarioByID(@Query("id") int id);

    @GET("api/users/all")
    Call<List<Usuario>> getAllUsuarios();

    @PUT("api/users/multar")
    Call<Usuario> multar(@Query("idUsuario") int idUsuario);



}
