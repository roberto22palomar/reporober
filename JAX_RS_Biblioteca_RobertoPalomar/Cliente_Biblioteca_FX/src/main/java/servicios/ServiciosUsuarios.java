package servicios;

import dao.DaoUsuarios;
import dao.modelos.Usuario;
import io.vavr.control.Either;

import javax.inject.Inject;
import java.util.List;

/**
 * @author : Rober Palomar
 */
public class ServiciosUsuarios {

    @Inject
    private DaoUsuarios du;

    public Either<String, Usuario> getUsuarioByID(int id) {
        return du.getUsuario(id);
    }

    public Either<String, List<Usuario>> getAllUsuario() {
        return du.getAllUsuarios();
    }

    public Either<String, Usuario> multar(int idUsuario) {
        return du.multar(idUsuario);
    }


}
