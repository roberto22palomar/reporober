package servicios;

import dao.DaoLibros;
import dao.modelos.Libro;
import dao.modelos.Prestamo;
import io.vavr.control.Either;

import javax.inject.Inject;
import java.util.List;

/**
 * @author : Rober Palomar
 */
public class ServiciosLibros {

    @Inject
    private DaoLibros dl;

    public Either<String, Libro> getLibrobyID(int id) {
        return dl.getLibroByID(id);
    }

    public Either<String, List<Libro>> getAllLibros() {
        return dl.getAllLibros();
    }

    public Libro addPrestamo(Prestamo prestamo) {
        return dl.addPrestamo(prestamo);
    }

    public boolean deletePrestamo(int idLibro, int idUsuario) {
        return dl.deletePrestamo(idLibro, idUsuario);
    }

    public List<Prestamo> getPrestamosUsuario(int idUsuario) {
        return dl.getPrestamosUsuario(idUsuario);
    }


}
