package errores;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class ValidationException extends Exception {
    private String error;
    private LocalDateTime fechaError;

    public ValidationException(String error) {
        this.error = error;
        this.fechaError = LocalDateTime.now();
    }
}
