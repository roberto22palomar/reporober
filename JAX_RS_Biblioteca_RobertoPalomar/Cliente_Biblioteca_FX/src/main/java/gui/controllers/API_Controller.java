package gui.controllers;

import dao.modelos.Libro;
import dao.modelos.Prestamo;
import dao.modelos.Usuario;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import servicios.ServiciosLibros;
import servicios.ServiciosUsuarios;

import javax.inject.Inject;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;


/**
 * @author : Rober Palomar
 */
public class API_Controller implements Initializable {

    @Inject
    private ServiciosUsuarios su;
    @Inject
    private ServiciosLibros sl;

    @FXML
    private ListView<Usuario> listviewUsuarios;
    @FXML
    private ListView<Libro> listviewLibros;
    @FXML
    private ListView<Prestamo> listviewPrestamos;
    @FXML
    private CheckBox checkbox;

    private Alert a;


    public void cargarUsuarios() {

        su.getAllUsuario().peek(u -> {
            listviewUsuarios.getItems().clear();
            listviewUsuarios.getItems().addAll(su.getAllUsuario().get());
        }).peekLeft(s -> {
            alertError(s);
        });


    }

    public void cargarLibros() {

        sl.getAllLibros().peek(l -> {
            listviewLibros.getItems().clear();
            listviewLibros.getItems().addAll(sl.getAllLibros().get());
        }).peekLeft(s -> {
            alertError("No hay libros que cargar");
        });


    }

    public void addPrestamo() {

        if (!checkbox.isSelected()) {

            if (!listviewUsuarios.getSelectionModel().getSelectedItem().isMulta()){
                if (listviewLibros.getSelectionModel().getSelectedItem() != null && listviewUsuarios.getSelectionModel().getSelectedItem() != null) {
                    Prestamo prestamo = Prestamo.builder()
                            .idLibro(listviewLibros.getSelectionModel().getSelectedItem().getId())
                            .idUsuario(listviewUsuarios.getSelectionModel().getSelectedItem().getId())
                            .fechaPrestamo(LocalDate.now())
                            .fechaDevolucion(LocalDate.now().plusDays(10))
                            .build();

                    if (sl.addPrestamo(prestamo) == null) {
                        alertError("No se ha añadido el prestamo");
                    }
                } else {
                    alertError("Debes seleccionar un usuario y un libro");
                }
            }else{
                alertError("Este usuario no puede alquilar al tener una multa activa");
            }


        }

    }

    public void getPrestamosUsuarios() {
        if (checkbox.isSelected()) {
            if (sl.getPrestamosUsuario(listviewUsuarios.getSelectionModel().getSelectedItem().getId()) != null) {
                listviewPrestamos.getItems().clear();
                listviewPrestamos.getItems().addAll(sl.getPrestamosUsuario(listviewUsuarios.getSelectionModel().getSelectedItem().getId()));
            } else {
                alertError("No hay prestamos del usuario");
            }
        }

    }

    public void deletePrestamo() {
        Prestamo p = listviewPrestamos.getSelectionModel().getSelectedItem();
        Usuario u = listviewUsuarios.getSelectionModel().getSelectedItem();
        if (p.getFechaDevolucion().isAfter(p.getFechaPrestamo())) {
            listviewUsuarios.getItems().remove(u);
            su.multar(u.getId()).peek(m -> {
                listviewUsuarios.getItems().add(m);
            });


            alertInfo("El usuario ha sido multado");
        } else {
            if (sl.deletePrestamo(p.getIdLibro(), p.getIdUsuario())) {
                alertInfo("Se ha devuelto correctamente");
            } else {
                alertError("No se ha devuelto correctamente");
            }
        }

    }

    public void alertError(String msg) {
        a.setTitle("BIBLIOTECA");
        a.setContentText(msg);
        a.showAndWait();
    }

    public void alertInfo(String msg) {
        a.setAlertType(Alert.AlertType.INFORMATION);
        a.setTitle("BIBLIOTECA");
        a.setContentText(msg);
        a.showAndWait();
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        cargarUsuarios();
        cargarLibros();
        a = new Alert(Alert.AlertType.ERROR);


    }
}
