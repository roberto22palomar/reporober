package configRetrofit;

import com.google.gson.*;
import config.ConfigurationSingleton_Client;
import lombok.extern.log4j.Log4j2;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.lang.reflect.Type;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.time.Duration;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Log4j2
public class ConfigurationSingleton_Retrofit {
    private static OkHttpClient clientOK;
    private static Retrofit retrofit;

    private ConfigurationSingleton_Retrofit() {
    }

    public static Retrofit getInstance() {
        if (clientOK == null) {
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

            clientOK = new OkHttpClient.Builder()
                    .readTimeout(Duration.of(10, ChronoUnit.MINUTES))
                    .callTimeout(Duration.of(10, ChronoUnit.MINUTES))
                    .connectTimeout(Duration.of(10, ChronoUnit.MINUTES))
                    .addInterceptor(chain -> {
                                Request original = chain.request();

                                Request.Builder builder1 = original.newBuilder();
                                Request request = builder1.build();
                                return chain.proceed(request);
                            }
                    )
                    .cookieJar(new JavaNetCookieJar(cookieManager))
                    .build();
        }
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(LocalDate.class, new JsonDeserializer<LocalDate>() {
                    @Override
                    public LocalDate deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                        return LocalDate.parse(jsonElement.getAsJsonPrimitive().getAsString());
                    }
                }).registerTypeAdapter(LocalDate.class, new JsonSerializer<>() {
                    @Override
                    public JsonElement serialize(Object o, Type type, JsonSerializationContext jsonSerializationContext) {
                        return new JsonPrimitive(LocalDate.now().toString());
                    }
                })
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(ConfigurationSingleton_Client.getInstance().getPath_base())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(clientOK)
                .build();

        return retrofit;
    }
}
