package dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import config.ConfigurationSingleton_Client;
import dao.modelos.Libro;
import dao.modelos.Prestamo;
import dao.utilsDao.ConfigurationSingleton_OkHttpClient;
import io.vavr.control.Either;
import retrofit.APILibros;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import configRetrofit.ConfigurationSingleton_Retrofit;


import java.util.List;

/**
 * @author : Rober Palomar
 */
public class DaoLibros {

    public Either<String, Libro> getLibroByID(int id) {
        Either<String, Libro> resultado = null;

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigurationSingleton_Client.getInstance().getPath_base())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(ConfigurationSingleton_OkHttpClient.getInstance())
                .build();

        APILibros librosAPI = retrofit.create(APILibros.class);

        Call<Libro> call = librosAPI.getLibrosByID(id);
        try {
            Response<Libro> response = call.execute();
            if (response.isSuccessful()) {
                Libro changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }


    public Either<String, List<Libro>> getAllLibros() {
        Either<String, List<Libro>> resultado = null;

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigurationSingleton_Client.getInstance().getPath_base())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(ConfigurationSingleton_OkHttpClient.getInstance())
                .build();

        APILibros librosAPI = retrofit.create(APILibros.class);

        Call<List<Libro>> call = librosAPI.getAllLibros();
        try {
            Response<List<Libro>> response = call.execute();
            if (response.isSuccessful()) {
                List<Libro> changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;


    }

    public Libro addPrestamo(Prestamo prestamo) {
        Libro resultado = null;

        APILibros librosAPI = ConfigurationSingleton_Retrofit.getInstance().create(APILibros.class);

        Call<Libro> call = librosAPI.addPrestamo(prestamo);
        try {
            Response<Libro> response = call.execute();
            if (response.isSuccessful()) {
                Libro changesList = response.body();
                resultado = changesList;
            } else {
                resultado = null;
            }
        } catch (Exception e) {
            resultado = null;
        }

        return resultado;

    }

    public List<Prestamo> getPrestamosUsuario(int idUsuario) {
        List<Prestamo> resultado = null;

        APILibros librosAPI = ConfigurationSingleton_Retrofit.getInstance().create(APILibros.class);

        Call<List<Prestamo>> call = librosAPI.getPrestamosUsuario(idUsuario);
        try {
            Response<List<Prestamo>> response = call.execute();
            if (response.isSuccessful()) {
                List<Prestamo> changesList = response.body();
                resultado = changesList;
            } else {
                resultado = null;
            }
        } catch (Exception e) {
            resultado = null;
        }

        return resultado;
    }

    public Boolean deletePrestamo(int idLibro, int idUsuario) {
       boolean resultado = false;

        APILibros librosAPI = ConfigurationSingleton_Retrofit.getInstance().create(APILibros.class);

        Call<Boolean> call = librosAPI.deletePrestamo(idLibro, idUsuario);
        try {
            Response<Boolean> response = call.execute();
            if (response.isSuccessful()) {
                boolean changesList = response.body();
                resultado = changesList;
            } else {
                resultado = false;
            }
        } catch (Exception e) {
            resultado = false;
        }

        return resultado;
    }


}
