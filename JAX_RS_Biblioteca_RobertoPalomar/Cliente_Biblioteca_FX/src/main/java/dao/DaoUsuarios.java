package dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import config.ConfigurationSingleton_Client;
import dao.modelos.Usuario;
import dao.utilsDao.ConfigurationSingleton_OkHttpClient;
import io.vavr.control.Either;
import retrofit.APIUsuarios;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

/**
 * @author : Rober Palomar
 */
public class DaoUsuarios {

    public Either<String, Usuario> getUsuario(int id) {
        Either<String, Usuario> resultado = null;

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigurationSingleton_Client.getInstance().getPath_base())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(ConfigurationSingleton_OkHttpClient.getInstance())
                .build();

        APIUsuarios usuariosAPI = retrofit.create(APIUsuarios.class);

        Call<Usuario> call = usuariosAPI.getUsuarioByID(id);
        try {
            Response<Usuario> response = call.execute();
            if (response.isSuccessful()) {
                Usuario changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }


    public Either<String, List<Usuario>> getAllUsuarios() {
        Either<String, List<Usuario>> resultado = null;

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigurationSingleton_Client.getInstance().getPath_base())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(ConfigurationSingleton_OkHttpClient.getInstance())
                .build();

        APIUsuarios usuariosAPI = retrofit.create(APIUsuarios.class);

        Call<List<Usuario>> call = usuariosAPI.getAllUsuarios();
        try {
            Response<List<Usuario>> response = call.execute();
            if (response.isSuccessful()) {
                List<Usuario> changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;


    }

    public Either<String,Usuario> multar(int idUsuario) {
        Either<String, Usuario> resultado = null;

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConfigurationSingleton_Client.getInstance().getPath_base())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(ConfigurationSingleton_OkHttpClient.getInstance())
                .build();

        APIUsuarios usuariosAPI = retrofit.create(APIUsuarios.class);

        Call<Usuario> call = usuariosAPI.multar(idUsuario);
        try {
            Response<Usuario> response = call.execute();
            if (response.isSuccessful()) {
                Usuario changesList = response.body();
                resultado = Either.right(changesList);
            } else {
                resultado = Either.left(response.errorBody().toString());
            }
        } catch (Exception e) {
            resultado = Either.left("Error de comunicacion");
        }

        return resultado;
    }



}
