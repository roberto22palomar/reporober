package dao;

import dao.modelos.Usuario;
import dao.utils.CifradoUtils;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class DaoUsuarios {

    private final CifradoUtils cifradoUtils = new CifradoUtils();

    public Either<String, Usuario> addUsuario(Usuario usuario) {
        Either<String, Usuario> resultado = null;

        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            session.save(usuario);

            cifradoUtils.certificadoYfichero(usuario);

            resultado = Either.right(usuario);

        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException) {
                resultado = Either.left("Usuario ya existe");
            } else {
                resultado = Either.left(e.getMessage());
            }
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }

    public Either<String, Usuario> getUsuario(String user) {

        Either<String, Usuario> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            Usuario a = session.bySimpleNaturalId(Usuario.class).load(user);

            if (a == null) {
                resultado = Either.left("Usuario no encontrado");
            } else {
                resultado = Either.right(a);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }

}
