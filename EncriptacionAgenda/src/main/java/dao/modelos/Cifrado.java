package dao.modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

/**
 * @author : Rober Palomar
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "mensajes")
public class Cifrado {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(name = "emisor")
    private String emisor;

    @Basic
    @NaturalId
    @Column(name = "destinatario")
    private String destinatario;

    @Basic
    @Column(name = "iv")
    private String iv;

    @Basic
    @Column(name = "salt")
    private String salt;

    @Basic
    @Column(name = "claveSimetrica")
    private String claveSimetrica;

    @Basic
    @Column(name = "mensaje")
    private String mensaje;

    @Basic
    @Column(name = "firma")
    private String firma;

    @Basic
    @Column(name = "iteraciones")
    private int iteraciones;


}
