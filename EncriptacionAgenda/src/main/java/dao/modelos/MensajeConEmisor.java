package dao.modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : Rober Palomar
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MensajeConEmisor {

    private String emisor;
    private String mensaje;

    @Override
    public String toString() {
        return "Mensaje de -> "+ emisor ;
    }
}
