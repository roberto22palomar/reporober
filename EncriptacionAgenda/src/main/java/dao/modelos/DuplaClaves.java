package dao.modelos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author : Rober Palomar
 */
@Data
@Builder
@AllArgsConstructor
public class DuplaClaves {

    PrivateKey clavePrivada;
    PublicKey clavePublica ;

}
