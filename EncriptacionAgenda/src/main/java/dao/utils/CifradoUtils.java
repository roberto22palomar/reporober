package dao.utils;

import dao.DaoMensajes;
import dao.modelos.Cifrado;
import dao.modelos.DuplaClaves;
import dao.modelos.MensajeConEmisor;
import dao.modelos.Usuario;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;

import javax.crypto.*;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class CifradoUtils {
    private final CrearParClaves claves = new CrearParClaves();
    private final CertificadoUtils certificadoUtils = new CertificadoUtils();
    private final KeyStoreUtil keyStoreUtil = new KeyStoreUtil();


    public Either<String, Cifrado> cifrarMensaje(String mensaje, Usuario emisor, String destinatario) {
        AtomicReference<Either<String, Cifrado>> resultado = new AtomicReference<>();
        SecureRandom sr = new SecureRandom();
        try {
            byte[] clave = new byte[16];
            byte[] iv = new byte[12];
            byte[] salt = new byte[16];

            sr.nextBytes(clave);
            String claveSimetrica = clave.toString();

            sr.nextBytes(iv);
            sr.nextBytes(salt);

            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            KeySpec spec = new PBEKeySpec(claveSimetrica.toCharArray(), salt, 65536, 256);
            SecretKey tmp = factory.generateSecret(spec);
            SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");


            Cipher cipher = Cipher.getInstance("AES/GCM/noPadding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);

            Cifrado c = Cifrado.builder()
                    .emisor(emisor.getUser())
                    .destinatario(destinatario)
                    .salt(Base64.getUrlEncoder().encodeToString(salt))
                    .iv(Base64.getUrlEncoder().encodeToString(iv))
                    .mensaje(Base64.getUrlEncoder().encodeToString(
                            cipher.doFinal(mensaje.getBytes(StandardCharsets.UTF_8))))
                    .iteraciones(65536)
                    .build();

            Cipher cifrador = Cipher.getInstance("RSA");

            AtomicReference<PublicKey> clavePublica = new AtomicReference<>();

            //COGER CLAVE PUBLICA DEL DESTINATARIO
            keyStoreUtil.leerKeyStorePublica(destinatario).peek(p -> {
                clavePublica.set(p);

                try {

                    //ENCRIPTAR CLAVE SIMETRICA
                    cifrador.init(Cipher.ENCRYPT_MODE, clavePublica.get());
                    c.setClaveSimetrica(Base64.getUrlEncoder()
                            .encodeToString(cifrador.doFinal(claveSimetrica.getBytes())));

                    AtomicReference<PrivateKey> miClavePrivada = new AtomicReference<>();

                    keyStoreUtil.leerKeyStorePrivado(emisor).peek(key -> {
                        miClavePrivada.set(key);

                        try {
                            //FIRMAR
                            Signature sign = Signature.getInstance("SHA256WithRSA");
                            sign.initSign(miClavePrivada.get());
                            sign.update(mensaje.getBytes());
                            c.setFirma(Base64.getUrlEncoder().encodeToString(sign.sign()));

                            //SE SUBE A BBDD EL MENSAJE YA COMPLETO
                            DaoMensajes dm = new DaoMensajes();

                            //SI ES CORRECTO SE DEVUELVE EL MENSAJE
                            dm.addMensaje(c).peek(m -> {
                                resultado.set(Either.right(m));
                            }).peekLeft(s -> {
                                resultado.set(Either.left(s));
                            });

                        } catch (NoSuchAlgorithmException e ) {
                            log.error(e.getMessage(), e);
                            resultado.set(Either.left(e.getMessage()));
                        } catch (SignatureException e) {
                            log.error(e.getMessage(), e);
                            resultado.set(Either.left(e.getMessage()));
                        } catch (InvalidKeyException e) {
                            log.error(e.getMessage(), e);
                            resultado.set(Either.left(e.getMessage()));
                        }

                    }).peekLeft(s -> {
                        resultado.set(Either.left(s));
                    });


                } catch (InvalidKeyException e) {
                    log.error(e.getMessage(), e);
                    resultado.set(Either.left(e.getMessage()));
                } catch (IllegalBlockSizeException e) {
                    log.error(e.getMessage(), e);
                    resultado.set(Either.left(e.getMessage()));
                } catch (BadPaddingException e) {
                    log.error(e.getMessage(), e);
                    resultado.set(Either.left(e.getMessage()));
                }

            }).peekLeft(s -> {
                resultado.set(Either.left(s));
            });


        } catch (InvalidKeyException e) {
            log.error(e.getMessage(), e);
            resultado.set(Either.left(e.getMessage()));
        } catch (InvalidAlgorithmParameterException e) {
            log.error(e.getMessage(), e);
            resultado.set(Either.left(e.getMessage()));
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
            resultado.set(Either.left(e.getMessage()));
        } catch (InvalidKeySpecException e) {
            log.error(e.getMessage(), e);
            resultado.set(Either.left(e.getMessage()));
        } catch (IllegalBlockSizeException e) {
            log.error(e.getMessage(), e);
            resultado.set(Either.left(e.getMessage()));
        } catch (BadPaddingException e) {
            log.error(e.getMessage(), e);
            resultado.set(Either.left(e.getMessage()));
        } catch (NoSuchPaddingException e) {
            log.error(e.getMessage(), e);
            resultado.set(Either.left(e.getMessage()));
        }

        return resultado.get();

    }

    public Either<String, List<MensajeConEmisor>> descrifrarMensaje(Usuario destinatario) {
        AtomicReference<Either<String, List<MensajeConEmisor>>> resultado = new AtomicReference<>();
        DaoMensajes dm = new DaoMensajes();
        AtomicReference<List<Cifrado>> listaCifrados = new AtomicReference<>();

        //LISTA DE TODOS LOS MENSAJES PARA EL USUARIO
        dm.getMensajes(destinatario.getUser()).peek(m -> {
            listaCifrados.set(m);
            List<MensajeConEmisor> mensajes = new ArrayList<>();

            for (int i = 0; i < listaCifrados.get().size(); i++) {
                AtomicReference<Cifrado> c = new AtomicReference<>();
                c.set(listaCifrados.get().get(i));

                //RECUPERAR CLAVE PRIVADA
                AtomicReference<PrivateKey> miClavePrivada = new AtomicReference<>();
                keyStoreUtil.leerKeyStorePrivado(destinatario).peek(p -> {
                    miClavePrivada.set(p);

                    try {

                        //DESCIFRAR LA CLAVE SIMETRICA EN BASE A LA CLAVE PRIVADA
                        Cipher cifrador = Cipher.getInstance("RSA");
                        cifrador.init(Cipher.DECRYPT_MODE, miClavePrivada.get());
                        String claveSimetrica = new String(cifrador.doFinal(
                                Base64.getUrlDecoder().decode(c.get().getClaveSimetrica())));

                        //DESCIFRAR MENSAJE
                        byte[] iv = Base64.getUrlDecoder().decode(c.get().getIv());
                        byte[] salt = Base64.getUrlDecoder().decode(c.get().getSalt());
                        int numeroIteraciones = c.get().getIteraciones();

                        //PARAMETROS PARA LA DESENCRIPTACION
                        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
                        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
                        KeySpec spec = new PBEKeySpec(claveSimetrica.toCharArray(), salt, numeroIteraciones, 256);
                        SecretKey tmp = factory.generateSecret(spec);
                        SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

                        //CRIFRADOR
                        Cipher cipher = Cipher.getInstance("AES/GCM/noPADDING");
                        cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);
                        String mensaje = new String(cipher.doFinal(
                                Base64.getUrlDecoder().decode(c.get().getMensaje())));

                        //RECUPERAR CLAVE PUBLICA DEL EMISOR
                        AtomicReference<PublicKey> claveEmisor = new AtomicReference<>();
                        keyStoreUtil.leerKeyStorePublica(c.get().getEmisor()).peek(key -> {
                            claveEmisor.set(key);

                            try {
                                //FIRMA
                                Signature sign = Signature.getInstance("SHA256WithRSA");
                                sign.initVerify(claveEmisor.get());
                                sign.update(mensaje.getBytes());

                                //COMPROBAR FIRMA
                                if (sign.verify(Base64.getUrlDecoder().decode(c.get().getFirma()))) {
                                    //DEVOLVER EL MENSAJE
                                    //MensajeConEmisor es un objeto que solo contiene el mensaje ya legible y el emisor
                                    MensajeConEmisor mce = MensajeConEmisor.builder()
                                            .emisor(c.get().getEmisor())
                                            .mensaje(mensaje)
                                            .build();
                                    mensajes.add(mce);

                                    resultado.set(Either.right(mensajes));
                                } else {
                                    resultado.set(Either.left("La firma no coincide"));
                                }
                            } catch (NoSuchAlgorithmException e) {
                                log.error(e.getMessage(), e);
                                resultado.set(Either.left(e.getMessage()));
                            } catch (SignatureException e) {
                                log.error(e.getMessage(), e);
                                resultado.set(Either.left(e.getMessage()));
                            } catch (InvalidKeyException e) {
                                log.error(e.getMessage(), e);
                                resultado.set(Either.left(e.getMessage()));
                            }


                        }).peekLeft(s -> {
                            resultado.set(Either.left(s));
                        });
                    } catch (InvalidKeyException e) {
                        log.error(e.getMessage(), e);
                        resultado.set(Either.left(e.getMessage()));
                    } catch (NoSuchAlgorithmException e) {
                        log.error(e.getMessage(), e);
                        resultado.set(Either.left(e.getMessage()));
                    } catch (InvalidAlgorithmParameterException e) {
                        log.error(e.getMessage(), e);
                        resultado.set(Either.left(e.getMessage()));
                    } catch (InvalidKeySpecException e) {
                        log.error(e.getMessage(), e);
                        resultado.set(Either.left(e.getMessage()));
                    } catch (IllegalBlockSizeException e) {
                        log.error(e.getMessage(), e);
                        resultado.set(Either.left(e.getMessage()));
                    } catch (BadPaddingException e) {
                        log.error(e.getMessage(), e);
                        resultado.set(Either.left(e.getMessage()));
                    } catch (NoSuchPaddingException e) {
                        log.error(e.getMessage(), e);
                        resultado.set(Either.left(e.getMessage()));
                    }


                }).peekLeft(s -> {
                    resultado.set(Either.left(s));
                });

            }
        }).peekLeft(s -> {
            resultado.set(Either.left(s));
        });

        return resultado.get();
    }


    public Either<String, Boolean> certificadoYfichero(Usuario usuario) {

        DuplaClaves parClaves = claves.generarParClaves().get();

        return certificadoUtils.generarCertificado(usuario.getUser(), usuario.getPassword(), parClaves.getClavePublica(), parClaves.getClavePrivada());
    }
}