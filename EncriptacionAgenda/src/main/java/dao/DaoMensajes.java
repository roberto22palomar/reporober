package dao;

import dao.modelos.Cifrado;
import io.vavr.control.Either;
import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import utils.HibernateUtilsSingleton;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * @author : Rober Palomar
 */
@Log4j2
public class DaoMensajes {

    //SUBIR UN OBJETO CIFRADO A BBDD
    public Either<String, Cifrado> addMensaje(Cifrado cifrado) {
        Either<String, Cifrado> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            session.save(cifrado);
            resultado = Either.right(cifrado);

        } catch (ConstraintViolationException e) {
            if (e.getCause() instanceof SQLIntegrityConstraintViolationException) {
                resultado = Either.left("Cifrado ya existe");
            } else {
                resultado = Either.left(e.getMessage());
            }
            log.error(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;


    }

    //COGER TODOS LOS MENSAJES DE UN USUARIO
    public Either<String, List<Cifrado>> getMensajes(String destinatario) {

        Either<String, List<Cifrado>> resultado = null;
        try (Session session = HibernateUtilsSingleton.getInstance().getSession()) {

            List<Cifrado> cifrados = session.createQuery("from Cifrado where destinatario=:destinatario",Cifrado.class)
                    .setParameter("destinatario",destinatario).getResultList();

            if (cifrados.isEmpty()) {
                resultado = Either.left("No hay mensajes para este usuario");
            } else {
                resultado = Either.right(cifrados);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            resultado = Either.left(e.getMessage());
        }
        return resultado;

    }




}
