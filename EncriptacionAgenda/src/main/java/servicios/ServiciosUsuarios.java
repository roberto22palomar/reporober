package servicios;

import dao.DaoUsuarios;
import dao.modelos.Usuario;
import dao.utils.PasswordHash;
import io.vavr.control.Either;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * @author : Rober Palomar
 */
public class ServiciosUsuarios {


    private final DaoUsuarios du = new DaoUsuarios();

    public Either<String, Usuario> addUsuario(Usuario usuario) throws InvalidKeySpecException, NoSuchAlgorithmException {

        usuario.setPassword(PasswordHash.createHash(usuario.getPassword()));//ENVIAR AL DAO YA LA CONTRASEÑA HASHEADA
        return du.addUsuario(usuario);
    }

    public Either<String, Usuario> getUsuario(String user) {
        return du.getUsuario(user);
    }

}
