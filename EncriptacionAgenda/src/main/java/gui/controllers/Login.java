package gui.controllers;

import dao.modelos.Usuario;
import dao.utils.PasswordHash;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import lombok.extern.log4j.Log4j2;
import servicios.ServiciosUsuarios;

import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ResourceBundle;

@Log4j2
public class Login implements Initializable {


    Usuario usuario = null;
    @FXML
    private TextField textFieldUser;
    @FXML
    private PasswordField textFieldPassword;
    @FXML
    private Alert info;
    @FXML
    private Alert error;
    @FXML
    private CheckBox checkboxRegister;
    @FXML
    private TextField textfieldUserR;
    @FXML
    private TextField textfieldPasswordR;
    @FXML
    private TextField textfieldMailR;
    @FXML
    private Label labelUser;
    @FXML
    private Label labelPass;
    @FXML
    private Label labelMail;
    @FXML
    private Button botonRegister;
    private PantallaPrincipal principalController;

    ServiciosUsuarios su = new ServiciosUsuarios();

    public void login() throws InvalidKeySpecException, NoSuchAlgorithmException {

        if (!textFieldUser.getText().isEmpty() && !textFieldPassword.getText().isEmpty()) {
            su.getUsuario(textFieldUser.getText()).peek(u -> {
                usuario = u;
                String passIntroducida = textFieldPassword.getText();
                String passBBDD = usuario.getPassword();

                try {
                    if (PasswordHash.validatePassword(passIntroducida, passBBDD)) {
                        principalController.setUserLogeado(usuario);
                        principalController.preCargaBandejaEntrada();
                    } else {
                        alertError("Contraseña y/o Usuario incorrectos");
                    }
                } catch (NoSuchAlgorithmException e) {
                    log.error(e.getMessage(), e);
                } catch (InvalidKeySpecException e) {
                    log.error(e.getMessage(), e);
                }

            }).peekLeft(s -> {
                alertError(s);
            });

        } else {
            alertError("Debes rellenar todos los campos");
        }
    }

    public void registrar() throws InvalidKeySpecException, NoSuchAlgorithmException {
        if (!textfieldUserR.getText().isEmpty()
                && !textfieldPasswordR.getText().isEmpty()
                && !textfieldMailR.getText().isEmpty()) {

            Usuario usuario = Usuario.builder()
                    .id(0)
                    .user(textfieldUserR.getText())
                    .password(textfieldPasswordR.getText())
                    .mail(textfieldMailR.getText())
                    .build();

            su.addUsuario(usuario).peek(u -> {
                alertInfo("Se ha añadido correctamente el usuario");
            }).peekLeft(s -> {
                alertError(s);
            });

        } else {
            alertError("Ningun campo puede estar vacio");
        }
    }

    public void checkboxRegister() {
        if (checkboxRegister.isSelected()) {
            textfieldUserR.setVisible(true);
            textfieldPasswordR.setVisible(true);
            textfieldMailR.setVisible(true);
            botonRegister.setVisible(true);
            labelUser.setVisible(true);
            labelPass.setVisible(true);
            labelMail.setVisible(true);

        } else {
            textfieldUserR.setVisible(false);
            textfieldPasswordR.setVisible(false);
            textfieldMailR.setVisible(false);
            botonRegister.setVisible(false);
            labelUser.setVisible(false);
            labelPass.setVisible(false);
            labelMail.setVisible(false);
        }
    }


    private void alertInfo(String alertText) {
        info.setHeaderText("Login");
        info.setContentText(alertText);
        info.showAndWait();
    }

    private void alertError(String alertText) {
        error.setHeaderText("Login");
        error.setContentText(alertText);
        error.showAndWait();
    }

    public void cargarPrincipalController(PantallaPrincipal principal) {
        principalController = principal;
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        info = new Alert(Alert.AlertType.INFORMATION);
        error = new Alert(Alert.AlertType.ERROR);

    }


}
