package gui.controllers;

import dao.modelos.MensajeConEmisor;
import dao.utils.CifradoUtils;
import io.reactivex.Single;
import io.reactivex.rxjavafx.schedulers.JavaFxScheduler;
import io.reactivex.schedulers.Schedulers;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import servicios.ServiciosUsuarios;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author : Rober Palomar
 */
public class BandejaEntrada implements Initializable {

    private PantallaPrincipal principalController;

    @FXML
    private ListView<MensajeConEmisor> listviewMensajes;
    @FXML
    private TextArea textAreaMensajes;
    @FXML
    private TextField textfieldDestinatario;
    @FXML
    private TextArea textfieldMensaje;

    private Alert error;
    private Alert info;

    @FXML
    private AnchorPane pantalla;

    private final CifradoUtils cifradoUtils = new CifradoUtils();
    private final ServiciosUsuarios su = new ServiciosUsuarios();

    public void cargarMensajes() {

        pantalla.setCursor(Cursor.WAIT);
        Single.just(
                        cifradoUtils.descrifrarMensaje(principalController.getUserLogeado()))
                .subscribeOn(Schedulers.io())
                .observeOn(JavaFxScheduler.platform())
                .doFinally(() -> pantalla.setCursor(Cursor.DEFAULT))
                .subscribe(cargarMensajes -> {

                            cargarMensajes.peek(mensajes -> {
                                listviewMensajes.getItems().addAll(mensajes);
                            }).peekLeft(s -> {
                                alertError(s);
                            });
                        },
                        throwable -> {
                            alertError(throwable.getMessage());
                        });
    }

    public void mandarMensaje() {

        if (!textfieldDestinatario.getText().isBlank() && !textfieldMensaje.getText().isBlank()) {

            su.getUsuario(textfieldDestinatario.getText()).peek(u -> {

                        pantalla.setCursor(Cursor.WAIT);
                        Single.just(
                                        cifradoUtils.cifrarMensaje(textfieldMensaje.getText(), principalController.getUserLogeado(), textfieldDestinatario.getText()))
                                .subscribeOn(Schedulers.io())
                                .observeOn(JavaFxScheduler.platform())
                                .doFinally(() -> pantalla.setCursor(Cursor.DEFAULT))
                                .subscribe(cifrarMensaje -> {
                                            cifrarMensaje.peek(enviado -> {
                                                alertInfo("Mensaje enviado con exito");
                                            }).peekLeft(s -> {
                                                alertError(s);
                                            });
                                        },
                                        throwable -> {
                                            alertError(throwable.getMessage());
                                        });
                    })
                    .peekLeft(s -> {
                        alertError("El destinatario no existe");
                    });
        } else {
            alertError("Ninguno de los campos puede estar vacio");
        }


    }

    public void logout() {
        listviewMensajes.getItems().clear();
        textAreaMensajes.clear();
        principalController.setUserLogeado(null);
        principalController.preCargaLogin();
        principalController.bandejaDeEntrada = null;


    }

    public void recargar() {
        listviewMensajes.getItems().clear();
        textAreaMensajes.clear();
        cargarMensajes();
    }

    public void mensajesAtextField() {
        MensajeConEmisor mensaje = listviewMensajes.getSelectionModel().getSelectedItem();

        if (mensaje != null) {
            textAreaMensajes.setText(mensaje.getMensaje());
        } else {
            alertError("Debes seleccionar un emisor para leer su mensaje.");
        }

    }


    public void cargarPrincipalController(PantallaPrincipal principal) {
        principalController = principal;
    }

    private void alertInfo(String alertText) {
        info.setHeaderText("Login");
        info.setContentText(alertText);
        info.showAndWait();
    }

    private void alertError(String alertText) {
        error.setHeaderText("Login");
        error.setContentText(alertText);
        error.showAndWait();
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        info = new Alert(Alert.AlertType.INFORMATION);
        error = new Alert(Alert.AlertType.ERROR);


    }
}
