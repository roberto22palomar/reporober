package gui.controllers;


import dao.modelos.Usuario;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PantallaPrincipal implements Initializable {


    public AnchorPane paneLogin;

    public AnchorPane bandejaDeEntrada;

    public BorderPane root;

    Usuario userLogeado;


    public Usuario getUserLogeado() {
        return userLogeado;
    }

    public void setUserLogeado(Usuario userLogeado) {
        this.userLogeado = userLogeado;
    }

    public void preCargaLogin() {
        if (paneLogin == null) {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource("/fxml/login.fxml"));
            try {
                paneLogin = loaderMenu.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Login loginController = loaderMenu.getController();
            loginController.cargarPrincipalController(this);

        }
        root.setCenter(paneLogin);
    }

    public void preCargaBandejaEntrada() {
        if (bandejaDeEntrada == null) {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource("/fxml/bandejaDeEntrada.fxml"));
            try {
                bandejaDeEntrada = loaderMenu.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            BandejaEntrada bandejaEntradaController = loaderMenu.getController();
            bandejaEntradaController.cargarPrincipalController(this);
            bandejaEntradaController.cargarMensajes();

        }

        root.setCenter(bandejaDeEntrada);

    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        preCargaLogin();
    }

}
